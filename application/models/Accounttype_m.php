<?php
class Accounttype_m extends MY_Model {
	// @todo - creat function store chart_of_account
	
	protected $_table_name = 'accounttype';
	protected $_order_by = 'catid';
	protected $_timestamps = TRUE;
	public $rules = array (
            'category' => array (
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'trim|required'
			),
	);
	function __construct() {
		parent::__construct();
	}
	public function delete($id){
		//delete a chart_of_account
		parent::delete($id);
		//then reset the parent it to 0
		//$this->db->set(array('parent_id'=>0))->where('parent_id',$id)->update($this->_table_name);
	}

	public function get_new() {
		$chart_of_account = new stdClass ();
		$chart_of_account->name = '';
			
		return $chart_of_account;
	}
}