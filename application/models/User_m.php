<?php
/**
 *
 */
class User_m extends MY_Model {
	protected $_table_name = 'users';
	protected $_order_by = 'name';
	protected $_myview = 'user_view';
	public $rules = array (
			'email' => array (
					'field' => 'email',
					'label' => 'Emial',
					'rules' => 'trim|required|valid_email'
			),
			'password' => array (
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'trim|required'
			)
	);
	public $rules_admin = array (
			'name' => array (
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'trim|required'
			),
			'email' => array (
					'field' => 'email',
					'label' => 'Emial',
					'rules' => 'trim|required|valid_email|is_unique[users.email]'
			),
			'password' => array (
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'trim|matches[password_confirm]'
			),
			'password_confirm' => array (
					'field' => 'password_confirm',
					'label' => 'Confirm Password',
					'rules' => 'trim|matches[password]'
			)
	);
	public $profile_rule = array (
        'name' => array (
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim'
        ),
    );
	function __construct() {
		parent::__construct ();
	}
	public function get_query($id=null){
	    $sql = "select us.id, us.`name`, us.username, us.email, us.`password`, us.active, g.`name` as role, g.feature from users_groups ug INNER JOIN users us on us.id = ug.user_id
INNER JOIN groups g on ug.`group` = g.id";
        if($id)
        $sql .= ' where us.id = '.$id;

        $query = $this->db->query($sql);
        return $query->result_object();
    }
	public function login() {
		$user = $this->get_by ( array (
				'email' => $this->input->post ( 'email' ),
				'password' => $this->hash ( $this->input->post ( 'password' ) )
		), TRUE );

		if (count ( $user )) {
			// log in user
			$data = array (
					'name' => $user->name,
					'email' => $user->email,
					'id' => $user->id,
					'loggedin' => TRUE
			);
			$this->session->set_userdata ( $data );

		}
	}
	public function loggedin() {
		return ( bool ) $this->session->userdata ( 'loggedin' );
	}
	public function logout() {
		$this->session->sess_destroy ();
	}
	public function get_new() {
		$user = new stdClass ();
		$user->name = '';
		$user->email = '';
		$user->password = '';
		$user->sex = '';
		$user->dob = '';
		$user->photo = '';
		$user->pob = '';
		$user->phone = '';

		return $user;
	}
    public function get_new_profile() {
        $user = new stdClass ();
        $user->name = '';
        $user->email = '';
        $user->password = '';
        $user->sex = '';
        $user->dob = '';
        $user->photo = '';
        $user->pob = '';
        $user->phone = '';
        $user->confirm_password = '';

        return $user;
    }
	public function hash($string) {
		return hash ( 'sha512', $string . config_item ( 'encryption_key' ) );
	}
}
