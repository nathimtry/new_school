<?php
class Vouchers_m extends MY_Model {
    // @todo - creat function store voucher

    protected $_table_name = 'vouchers';
    protected $_order_by = 'id';
    protected $_timestamps = TRUE;
    public $rules = array (
        'vdate' => array (
            'field' => 'vdate',
            'label' => 'vdate',
            'rules' => 'required'
        ),
    );
    function __construct() {
        parent::__construct();
    }
    public function delete($id){
        //delete a voucher
        parent::delete($id);
        //then reset the parent it to 0
        //$this->db->set(array('parent_id'=>0))->where('parent_id',$id)->update($this->_table_name);
    }

    public function get_new() {
        $voucher = new stdClass ();
        $voucher->vdate = '';
        $voucher->vtype = '';
        $voucher->acc_code = '';
        $voucher->auth_by = '';
        $voucher->pre_by = '';
        $voucher->amount = '';
        $voucher->narration = '';

        return $voucher;
    }
}