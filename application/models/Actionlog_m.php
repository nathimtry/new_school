<?php
class Actionlog_m extends MY_Model {
	protected $_table_name = 'action_log';
	protected $_order_by = 'id';
	protected $_timestamps = TRUE;
	public $rules = array (
			'action' => array (
					'field' => 'action',
					'label' => 'Name',
					'rules' => 'trim|required|xss_clean'
			),
			'name' => array (
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'trim|required|xss_clean'
			),
			'create_by' => array (
					'field' => 'create_by',
					'label' => 'Create By',
					'rules' => 'trim|intval'
			),
			'old_data' => array (
					'field' => 'old_data',
					'label' => 'backup_data',
					'rules' => 'trim|required'
			),
			'table_name' => array (
					'field' => 'table_name',
					'label' => 'Table name',
					'rules' => 'trim|required'
			)
	);
	function __construct() {
		parent::__construct();
	}
	
	
	public function get_new() {
		$page = new stdClass ();
		$page->action = '';
		$page->old_data = '';
		$page->create_by = '';
		$page->table_name = '';
		$page->name = '';
			
		return $page;
	}
}