<?php
class Student_m extends MY_Model {
	// @todo - creat function store student
	
	protected $_table_name = 'students';
	protected $_order_by = 'id';
	protected $_timestamps = TRUE;
	public $rules = array (
            'id' => array (
                'field' => 'id',
                'label' => 'ID',
                'rules' => 'trim'
            ),
			'en_name' => array (
					'field' => 'en_name',
					'label' => 'Latin Name',
					'rules' => 'trim|required'
			),
			'kh_name' => array (
					'field' => 'kh_name',
					'label' => 'Khmer Name',
					'rules' => 'trim|required'
			),
			'sex' => array (
					'field' => 'sex',
					'label' => 'Sex',
					'rules' => 'trim|required'
			),
            'dob' => array (
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => 'trim|required'
            ),
			'address' => array (
					'field' => 'address',
					'label' => 'Address',
					'rules' => 'trim'
			),
			'father_phone' => array (
					'field' => 'father_phone',
					'label' => 'father phone',
					'rules' => 'trim|is_natural'
			),
            'mother_phone' => array (
                'field' => 'mother_phone',
                'label' => 'mother phone',
                'rules' => 'trim|is_natural'
            ),
            'emergency_phone' => array (
                'field' => 'emergency_phone',
                'label' => 'emergency phone',
                'rules' => 'trim|is_natural'
            ),
            'pob' => array (
                'field' => 'pob',
                'label' => 'pob',
                'rules' => 'trim'
            ),

			'image' => array (
					'field' => 'image',
					'label' => 'Student image',
					'rules' => 'callback_file_check'
			),
			'remark' => array (
					'field' => 'remark',
					'label' => 'Remark',
					'rules' => 'trim'
			),
	);

	function __construct() {
		parent::__construct();
	}
	public function get_new() {
		$student = new stdClass ();
		$student->id = '';
		$student->en_name = '';
		$student->kh_name = '';
		$student->sex = '';
		$student->dob = '';
		$student->pob = '';
		$student->address = '';
		$student->father_name = '';
        $student->father_job = '';
        $student->father_phone= '';
        $student->mother_name = '';
        $student->mother_job = '';
        $student->mother_phone = '';
        $student->created = '';
		$student->student_image = '';
		$student->guardian = '';
		$student->emergency_phone = '';
		$student->remark = '';
		$student->image = '';

		return $student;
	}
    public function sarchstudent($str,$limit=null){
        $sql="SELECT * FROM {$this->_table_name} WHERE active='1' and en_name LIKE '%{$str}%' OR kh_name LIKE '%{$str}%' OR id like '{$str}' OR mobile_phone like '%{$str}%'";
        if($limit) $sql.=" limit {$limit}";
        $query = $this->db->query($sql);
        return $query->result_array();
//        return $sql;
    }
	
}