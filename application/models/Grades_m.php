<?php
class Grades_m extends MY_Model{
    function __construct() {
        parent::__construct();
    }
    protected $_table_name='grades';
    protected $_order_by='id';
    protected $_timestamps = TRUE;
    public $rules = array (
        'id' => array (
            'field' => 'id',
            'label' => 'ID',
            'rules' => 'trim'
        ),
        'level' => array (
            'field' => 'level',
            'label' => 'Level',
            'rules' => 'trim|required'
        ),
        'en_name' => array (
            'field' => 'en_name',
            'label' => 'Latin Name',
            'rules' => 'trim|required'
        ),
        'kh_name' => array (
            'field' => 'kh_name',
            'label' => 'Khmer Name',
            'rules' => 'trim|required'
        ),
        'duration' => array (
            'field' => 'duration',
            'label' => 'Duration',
            'rules' => 'trim|required'
        ),

    );

    public function get_new() {
        $grade = new stdClass ();
        $grade->id = '';
        $grade->level = '';
        $grade->en_name = '';
        $grade->kh_name = '';
        $grade->duration = '';

        return $grade;
    }
}