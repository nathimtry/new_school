<?php
/**
 *
 */
class Group_m extends MY_Model {
	protected $_table_name = 'groups';
	protected $_order_by = 'id';
	public $rules = array (
			'name' => array (
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'trim|required' 
			)
	);
	
	function __construct() {
		parent::__construct ();
	}
	
	public function get_new() {
		$group = new stdClass ();
		$group->name = '';
		
		return $group;
	}
}
