<?php
/**
 *
 */
class Users_Groups_m extends MY_Model {
	protected $_table_name = 'users_groups';
	protected $_order_by = 'id';
	public $rules = array (
			'user_id' => array (
					'field' => 'user_id',
					'label' => 'User_ID',
					'rules' => 'trim|intval' 
			),
			'group' => array (
					'field' => 'group',
					'label' => 'Group',
					'rules' => 'trim|intval' 
			)
	);
	
	function __construct() {
		parent::__construct ();
	}
	
	public function get_new() {
		$users_groups = new stdClass ();
		$users_groups->user_id = '';
		$users_groups->group = '';
		return $users_groups;
	}
}
