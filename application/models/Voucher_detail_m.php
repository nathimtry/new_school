<?php
class Voucher_detail_m extends MY_Model {
    // @todo - creat function store voucher

    protected $_table_name = 'voucher_details';
    protected $_order_by = 'id';
    protected $_timestamps = TRUE;
    public $rules = array (
        'vno' => array (
            'field' => 'vno',
            'label' => 'vno',
            'rules' => 'required'
        ),
        'narration' => array (
            'field' => 'narration',
            'label' => 'narration',
            'rules' => 'trim'
        ),
        'debit' => array (
            'field' => 'debit',
            'label' => 'debit',
            'rules' => 'trim|required'
        ),
        'auth_by' => array (
            'field' => 'auth_by',
            'label' => 'auth_by',
            'rules' => 'trim'
        ),
        'pre_by' => array (
            'field' => 'pre_by',
            'label' => 'pre_by',
            'rules' => 'trim'
        ),
        'narration' => array (
            'field' => 'narration',
            'label' => 'narration',
            'rules' => 'trim'
        ),
    );
    function __construct() {
        parent::__construct();
    }
    public function delete($id){
        //delete a voucher
        parent::delete($id);
        //then reset the parent it to 0
        //$this->db->set(array('parent_id'=>0))->where('parent_id',$id)->update($this->_table_name);
    }
    public function get_expense(){
        $this->db->select('voucher_details.*,chart_of_account.name,chart_of_account.category,chart_of_account.catid,')
            ->from('voucher_details')
            ->join('chart_of_account','chart_of_account.id=voucher_details.acc_code')
            ->where(array('chart_of_account.catid'=>1))
        ;
        $query = $this->db->get();
        return $query->result_object();
    }

    public function get_other_income(){
        $this->db->select('voucher_details.*,chart_of_account.name,chart_of_account.category,chart_of_account.catid,')
            ->from('voucher_details')
            ->join('chart_of_account','chart_of_account.id=voucher_details.acc_code')
            ->where(array('chart_of_account.id'=>10))
        ;
        $query = $this->db->get();
        return $query->result_object();
    }

    public function get_voucher_type($where){
        $this->db->select('voucher_details.*,sum(voucher_details.debit) as sum_debit, sum(voucher_details.credit) as sum_credit,  chart_of_account.name, chart_of_account.category, chart_of_account.catid, vouchers.vdate')
            ->from('voucher_details')
            ->join('vouchers','vouchers.id = voucher_details.vno')
            ->join('chart_of_account','chart_of_account.id=voucher_details.acc_code')
            ->where($where)
            ->group_by('voucher_details.acc_code')
//            ->where(array('chart_of_account.catid'=>$cid))
        ;
        $query = $this->db->get();
        return $query->result_object();
    }
    public function get_transfer($where){
        $this->db->select('voucher_details.*,sum(voucher_details.debit) as sum_debit, sum(voucher_details.credit) as sum_credit,  chart_of_account.name, chart_of_account.category, chart_of_account.catid, vouchers.vdate')
            ->from('voucher_details')
            ->join('vouchers','vouchers.id = voucher_details.vno')
            ->join('chart_of_account','chart_of_account.id=voucher_details.acc_code')
            ->where($where)
            ->group_by('vouchers.id')
//            ->where(array('chart_of_account.catid'=>$cid))
        ;
        $query = $this->db->get();
        return $query->result_object();
    }

    public function get_voucher($where){
        $this->db->select('voucher_details.*,chart_of_account.name, chart_of_account.category,  chart_of_account.type, chart_of_account.catid, vouchers.vdate')
            ->from('voucher_details')
            ->join('vouchers','vouchers.id = voucher_details.vno')
            ->join('chart_of_account','chart_of_account.id=voucher_details.acc_code')
            ->where($where)
//            ->where(array('chart_of_account.catid'=>$cid))
        ;
        $query = $this->db->get();
        return $query->result_object();
    }

    public function get_voucher_payment($where){
        $payments  = $this->get_voucher(array('vouchers.contract_id'=>$where));
        $sum = 0;
        $debit = 0;
        $credit = 0;
        foreach($payments as $item){
            if($item->acc_code==18 || $item->acc_code==19 ){
                $sum += $item->credit;

            }
        }
        return $sum;
    }
    public function get_ledger(){
        $this->db->select('voucher_details.*,chart_of_account.name,chart_of_account.category,chart_of_account.catid,')
            ->from('voucher_details')
            ->join('chart_of_account','chart_of_account.id=voucher_details.acc_code')
        ;
        $query = $this->db->get();
        return $query->result_object();
    }

    public function get_new() {
        $voucher = new stdClass ();
        $voucher->vdate = '';
        $voucher->vtype = '';
        $voucher->acc_code = '';
        $voucher->auth_by = '';
        $voucher->pre_by = '';
        $voucher->narration = '';

        return $voucher;
    }

}