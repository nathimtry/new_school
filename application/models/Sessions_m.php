<?php
class Sessions_m extends MY_Model {
    // @todo - creat function store sessions

    protected $_table_name = 'sessions';
    protected $_order_by = 'id';
    protected $_timestamps = TRUE;
    public $rules = array (
        'id' => array (
            'field' => 'id',
            'label' => 'ID',
            'rules' => 'trim'
        ),
        'en_name' => array (
            'field' => 'en_name',
            'label' => 'Latin Name',
            'rules' => 'trim|required'
        ),
        'kh_name' => array (
            'field' => 'kh_name',
            'label' => 'Khmer Name',
            'rules' => 'trim|required'
        ),

        'remark' => array (
            'field' => 'remark',
            'label' => 'Remark',
            'rules' => 'trim'
        ),
        'study_date' => array (
            'field' => 'study_date',
            'label' => 'Study Date',
            'rules' => 'trim|required'
        ),
        'study_time' => array (
            'field' => 'study_time',
            'label' => 'Study Time',
            'rules' => 'trim|required'
        ),
    );

    function __construct() {
        parent::__construct();
    }

    public function get_new() {
        $sessions = new stdClass ();
        $sessions->id = '';
        $sessions->en_name = '';
        $sessions->kh_name = '';
        $sessions->study_date = '';
        $sessions->study_time = '';

        return $sessions;
    }

    public function sarchsessions($str,$limit=null){
        $sql="SELECT * FROM {$this->_table_name} WHERE active='1' and en_name LIKE '%{$str}%' OR kh_name LIKE '%{$str}%' ";
        if($limit) $sql.=" limit {$limit}";
        $query = $this->db->query($sql);
        return $query->result_array();
//        return $sql;
    }

}