<?php

/**
 * Created by PhpStorm.
 * User: NaThim
 * Date: 3/15/2017
 * Time: 9:24 AM
 */

class Student extends Admin_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_m');
    }
    public function index()
    {
        $this->data['students'] = $this->student_m->get();
        $this->data['title'] = $this->lang->line('menu_student');
        $this->data['subview']='student/index';

        $this->run();

    }

    public function fine_student(){
        if(isset($_POST)){
            echo json_encode($this->student_m->sarchstudent($_POST['param']),20);
            exit;
        }else{
            return false;
        }
    }

    public function edit($id=null){
        //check a student new one
        if($id){
            $this->data['student'] = $this->student_m->get($id);
            count($this->data['student'])|| $this->data['errors']='student could not fine';
        }
        else{
            $this->data['student'] = $this->student_m->get_new();
        }
        // Set up the for for input data
        $rules = $this->student_m->rules;
        $this->form_validation->set_rules($rules);
        $config['encrypt_name'] = TRUE;
        $config['upload_path']          = 'uploads';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['overwrite'] = TRUE;
        $config['max_size']             = 100000;
        $config['max_width']            = 10240;
        $config['max_height']           = 7680;
        $file='image';
        $temp ='';
        if($this->form_validation->run() == TRUE) {
//			//upload images
            $pastval = $_POST;
            if(isset( $pastval['submit'])){unset( $pastval['submit']);}
            if(isset( $pastval->submit)){unset( $pastval->submit);}

            $field = field_post($pastval);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file)) {
                $error = array('error' => $this->upload->display_errors());
//                dump($error);
            } else {
                $data = array('upload_data' => $this->upload->data());
                $temp = $this->upload->data();
            }
            $image_name = $_FILES['image'];
            if ($image_name['name'] != '') {
//                $data = $this->student_m->array_from_post();
                $data = $this->student_m->array_from_post($field);
                $data['image'] = $temp['file_name'];
//                dump($temp);
            } else {
                $data = $this->student_m->array_from_post();
            }

            $data['dob'] = date('Y-m-d', strtotime($_REQUEST['dob']));
//            dump($data);
//            exit();
            $this->student_m->save($data, $id);
            redirect('student');
        }
        $this->data['subview']='student/edit';
        $this->run();
    }

    public function ajax_get_student(){
        $pro_id = $_POST['pro_id'];
        $des_id = $_POST['dec_id'];
        $data = $this->student_m->get_by(array('province'=>$pro_id, 'destrict'=>$des_id));
        echo json_encode($data);
    }
    public function add_ajax(){
        $data = $_POST;
        if($data['kh_name']!='' ||$data['mobile_phone'] !=''){
            if($this->student_m->save($data)){
                echo 'success';
            }else{
                echo 'fail';
            }
        }else{
            echo 'fail';
        }
    }
    public function ajax_get_student2(){
        $student_id = $_POST['student_id'];
        $data = $this->student_m->get($student_id);
        echo json_encode($data);
    }

    public function ajax_get(){
        $data = $this->student_m->get();
        echo json_encode($data);
    }
    public function delete($id) {
        $this->student_m->delete($id);
        redirect('student');
    }
    public function deactive($id = null) {
        //get all data from order
        $student = $_REQUEST['id'];
        foreach($student as $item){
            $item;
            $this->student_m->deactive($item);
        }
    }

}