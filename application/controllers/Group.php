<?php
class Group extends Admin_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->library('controllerlist');
	}
	public function index(){
		$this->data['subview'] = 'group/index';
		$this->data['groups'] = $this->group_m->get();
		$this->run();
	}
	
	public function edit($id = Null){
	    $this->data['controllers'] = $this->controllerlist->getControllers();
//	    var_dump($controllers);
		if($id){
			$this->data['group'] = $this->group_m->get($id);
			count($this->data['group'])|| $this->data['errors']='group could not fine';
		}
		else{
			$this->data['group'] = $this->group_m->get_new();
		}
		// Set up the for for input data
		$rules = $this->group_m->rules;
		$this->form_validation->set_rules($rules);
		//dump($this->group_m->generate_id());
		// Process the form
		if($this->form_validation->run() == TRUE){
//		    var_dump($_POST);
//		    exit();
		    $data['name'] = $_POST['name'];
		    $data['feature'] = serialize($_POST['feature']);
			
			$this->group_m->save($data, $id);
			redirect('group');
		}
		$this->data['subview'] = 'group/edit';
		$this->run();
	}
	public function delete($id) {
		if($id == $this->session->groupdata['id']){
			$this->session->set_flashdata('error', 'group is currently login!');
			redirect('group');
			return false;
				
		}
		$this->group_m->delete($id);
		redirect('group');
	}
}