<?php
class User extends Admin_Controller
{
    private $CI;
	public function __construct(){
		parent::__construct();
		$this->load->model('group_m');
		$this->load->model('users_groups_m');
		$this->load->model('user_setting_m');
	}
	public function index(){
//        dump($this->controllerlist->getControllers());
		$this->data['subview'] = 'user/index';
		$this->data['users'] = $this->user_m->get_query();
		$this->run();
	}
	public function login(){
		
	//redirect user if it login
		$dashboard = 'dashboard';
		$this->user_m->loggedin() == FALSE || redirect($dashboard);
		
		//set the form
		$rules = $this->user_m->rules;
		$this->form_validation->set_rules($rules);
		
		//process form
		if ($this->form_validation->run() == TRUE) {
			//we can login and redirect
			$this->user_m->login();
			if($this->user_m->login()== TRUE){
				redirect($dashboard);
			}
			else{
				$this->session->set_flashdata('error', 'wrong email or password, Please try again');
				redirect('user/login', 'refresh');
			}
		}
		//load the view
		$this->data['subview'] = "user/login";
		$this->run_model();
	}
	public function logout(){
		$this->user_m->logout();
		redirect('user/login');
	}
	public function edit($id = Null){
		$this->data['roles'] = $this->group_m->get();
		//check a user new one
		if($id){
			$this->data['user'] = $this->user_m->get_view($id);
			count($this->data['user'])|| $this->data['errors']='user could not fine';
		}
		else{
			$this->data['user'] = $this->user_m->get_new();
		}
	
		// Set up the for for input data
		$rules = $this->user_m->rules_admin;
		$id || $rules['password']['rules'] .= '|required';
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if($this->form_validation->run() == TRUE){
			$data = $this->user_m->array_from_post(array('name', 'email', 'password'));
			$data['password'] = $this->user_m->hash($data['password']);
			
			$data2['user_id'] = $this->user_m->save($data, $id);
			$data2['group'] =  $_REQUEST['role'];
			$this->users_groups_m->save($data2,$id);
			redirect('user');
		}
		//load subview
		$this->data['subview'] = 'user/edit';
		$this->run();
	}
	public function delete($id) {
		if($id == $this->session->userdata['id']){
			$this->session->set_flashdata('error', 'User is currently login!');
			redirect('user');
			return false;
				
		}
		$this->user_m->delete($id);
		redirect('user');
	}
	public function profile($id=null){
        $this->data['roles'] = $this->group_m->get();
        //check a user new one
        if($id){
            $this->data['user'] = $this->user_m->get($id);
            count($this->data['user'])|| $this->data['errors']='user could not fine';
        }
        else{
            show_error('This page is not available! <a href="'.site_url('dashboard').'">Back</a>',true, 'Permission Deny');
        }

        // Set up the for for input data
        $rules = $this->user_m->profile_rule;
        $id || $rules['password']['rules'] .= '|required';
        $this->form_validation->set_rules($rules);

        // Process the form
//        $config['uri_protocol'] = site_url() ;
        $config['encrypt_name'] = TRUE;
        $config['upload_path']          = 'uploads/user';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['overwrite'] = TRUE;
        $config['max_size']             = 100000;
        $config['max_width']            = 10240;
        $config['max_height']           = 7680;
//
        $file='image';
        $temp ='';
        if($this->form_validation->run() == TRUE){

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file)) {
                $error = array('error' => $this->upload->display_errors());
                dump($error);
            } else {
                $data = array('upload_data' => $this->upload->data());
                $temp = $this->upload->data();
            }

            $image_name = $_FILES['image'];
            if ($image_name['name'] != '') {

                $pastval = $_POST;
                if($_POST['password'] == ''){
                    unset($pastval['confirm_password']);
                    unset($pastval['password']);
                }else{
                    unset($pastval['confirm_password']);
                }
                $field = field_post($pastval);
                $data = $this->user_m->array_from_post($field);
                $data['password'] = $this->user_m->hash($data['password']);
                $data['photo'] = $temp['file_name'];
                $data['dob'] = date('Y-m-d', strtotime($_REQUEST['dob']));
//                echo dump($data);
//                exit();

                $this->user_m->save($data, $id);
            } else {
                $pastval = $_POST;
//                unset($pastval['cog']);
                if($_POST['password'] == ''){
                    unset($pastval['confirm_password']);
                    unset($pastval['password']);
                }else{
                    unset($pastval['confirm_password']);
                }
                $field = field_post($pastval);
                $data = $this->user_m->array_from_post($field);
                $data['password'] = $this->user_m->hash($data['password']);
                $data['dob'] = date('Y-m-d', strtotime($_REQUEST['dob']));
                $this->user_m->save($data, $id);
            }
            $this->session->set_flashdata('success', 'Updated information');
            redirect('user/profile/'.$id);
        }else{
            $this->session->set_flashdata('error', 'Not Allow');
        }


        //load subview
        $this->data['subview'] = 'user/profile';
        $this->run();
    }
    public function setting($id=null){
	    $this->data['capital_account'] =$this->chart_of_account_m->get_by(array('type'=>5));
	    $this->data['expense_account'] =$this->chart_of_account_m->get_by(array('type'=>1));
	    $this->data['income_account'] =$this->chart_of_account_m->get_by(array('type'=>2));
	    $this->data['cash'] =$this->chart_of_account_m->get_by(array('type'=>3, 'parent_id'=>1));
	    $this->data['item_setting'] = $this->item_setting->get();
	    $this->data['setting'] = $this->user_setting_m->get_by(array('user_id'=>$this->session->userdata['id']));

	    $rules = $this->user_setting_m->rules;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == TRUE){
            $item = $_POST['item'];
            $val = $_POST['value'];
            $this->user_setting_m->deletes(array('user_id'=>$this->session->userdata['id']));
            for($i=0;$i<count($item);$i++){
                $data['user_id'] =$this->session->userdata['id'];
                $data['item'] = $item[$i];
                $data['value'] = $val[$i];
                $this->user_setting_m->save($data);
            }
//            echo dump($data);
//            exit();
        }

        //load subview
        $this->data['subview'] = 'user/setting';
        $this->run();
    }
	public function _unique_email($str){
	
		// do not validate if email already exist
		// unless the email currently will be update the user
		$id = $this->uri->segment(3);
		$this->db->where('email', $this->input->post('email'));
		!$id || $this->db->where('id !=', $id);
		$user = $this->user_m->get();
		if (count($user)){
			$this->form_validation->set_message('_unique_email', '%s should be unique');
			return false;
		}
		return TRUE;
	}
}