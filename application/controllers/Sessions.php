<?php

/**
 * Created by PhpStorm.
 * User: NaThim
 * Date: 3/15/2017
 * Time: 9:24 AM
 */

class Sessions extends Admin_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sessions_m');
    }
    public function index()
    {
        $this->data['sessions'] = $this->sessions_m->get();
        $this->data['title'] = $this->lang->line('menu_session');
        $this->data['subview']='sessions/index';

        $this->run();

    }
    public function edit($id=null){
        //check a sessions new one
        if($id){
            $this->data['sessions'] = $this->sessions_m->get($id);
            count($this->data['sessions'])|| $this->data['errors']='sessions could not fine';
        }
        else{
            $this->data['sessions'] = $this->sessions_m->get_new();
        }
        // Set up the for for input data
        $rules = $this->sessions_m->rules;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == TRUE) {
            $pastval = $_POST;

            if(isset( $pastval['submit'])){unset( $pastval['submit']);}
//            if(isset( $pastval->submit)){unset( $pastval->submit);}

            $field = field_post2($pastval);
            $data = $this->sessions_m->array_from_post($field);
//            echo dump($data);
//                exit();
            $this->sessions_m->save($data, $id);
            redirect('sessions');
        }

        $this->data['subview']='sessions/edit';
        $this->run();
    }

//    public function ajax_get_student(){
//        $pro_id = $_POST['pro_id'];
//        $des_id = $_POST['dec_id'];
//        $data = $this->student_m->get_by(array('province'=>$pro_id, 'destrict'=>$des_id));
//        echo json_encode($data);
//    }
//    public function add_ajax(){
//        $data = $_POST;
//        if($data['kh_name']!='' ||$data['mobile_phone'] !=''){
//            if($this->student_m->save($data)){
//                echo 'success';
//            }else{
//                echo 'fail';
//            }
//        }else{
//            echo 'fail';
//        }
//    }
//    public function ajax_get_student2(){
//        $student_id = $_POST['student_id'];
//        $data = $this->student_m->get($student_id);
//        echo json_encode($data);
//    }

//    public function ajax_get(){
//        $data = $this->sessions_m->get();
//        echo json_encode($data);
//    }
//    public function delete($id) {
//        $this->sessions_m->delete($id);
//        redirect('sessions');
//    }

    public function deactive($id = null) {
        //get all data from order
        $sessions = $_REQUEST['id'];
        foreach($sessions as $item){
            $item;
            $this->sessions_m->deactive($item);
        }
    }
}