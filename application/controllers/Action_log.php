<?php
class Action_log extends Admin_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('actionlog_m');
	}
	function index(){
		$this->data['subview'] = 'action_log/index';
		$this->data['action_logs'] = $this->actionlog_m->get();
		$this->run();
	}
}