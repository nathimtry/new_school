<?php
class User_group extends Admin_Controller
{
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$this->data['subview'] = 'user_user_group/index';
		$this->data['user_groups'] = $this->user_group_m->get_view();
		$this->run();
	}
	
	public function edit($id = Null){
		if($id){
			$this->data['user_group'] = $this->user_group_m->get_view($id);
			count($this->data['user_group'])|| $this->data['errors']='user_group could not fine';
		}
		else{
			$this->data['user_group'] = $this->user_group_m->get_new();
		}
		// Set up the for for input data
		$rules = $this->user_group_m->rules;
		$this->form_validation->set_rules($rules);
		//dump($this->user_group_m->generate_id());
		// Process the form
		if($this->form_validation->run() == TRUE){
		
			$data = $this->user_group_m->array_from_post(array('name', 'feature'));
			
			$this->user_group_m->save($data, $id);
			redirect('user_group');
		}
		$this->data['subview'] = 'user_group/edit';
		$this->run();
	}
	public function delete($id) {
		if($id == $this->session->user_groupdata['id']){
			$this->session->set_flashdata('error', 'user_group is currently login!');
			redirect('user_group');
			return false;
				
		}
		$this->user_group_m->delete($id);
		redirect('user_group');
	}
	
}