<?php
class Dashboard extends Admin_Controller
{
    public function __construct(){
        parent::__construct();
    }
    public function index(){
        if($this->session->userdata['role'] == 'administrator'){
            $this->data['subview']='dashboard/index';

        }else{
            $this->data['subview']='dashboard/index';

        }
        $this->run();
    }

}