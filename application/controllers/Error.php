<?php
/**
 * 
 */
class Error extends Admin_Controller {
	
	public function __construct() {
		parent::__construct();

	}
	public function index(){
		$this->data['subview'] = 'error/index';
		$this->run();
	}
	public function deactive($id) {
		$this->uom_m->deactive($id);
		redirect('uom');
	}
	public function edit($id = Null){
		//check a uom new one
		if($id){
			$this->data['uom'] = $this->uom_m->get($id);
			count($this->data['uom'])|| $this->data['errors']='uom could not fine';
		}
		else{
			$this->data['uom'] = $this->uom_m->get_new();
		}
		
		//uoms for dropdown
		$this->data['uoms'] = $this->uom_m->get();
		// Set up the for for input data
		$rules = $this->uom_m->rules;
		$this->form_validation->set_rules($rules);
		
		// Process the form
		if($this->form_validation->run() == TRUE){
		    $field = field_post($_POST);
			$data = $this->uom_m->array_from_post($field);
			$this->uom_m->save($data, $id);
			redirect('uom');
		}
		//load subview
		$this->data['subview'] = 'uom/edit';
		$this->run();
	}
	public function delete($id) {
		$this->uom_m->delete($id);
		redirect('uom');
	}
	
}
