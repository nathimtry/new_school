<?php

/**
 * Created by PhpStorm.
 * User: NaThim
 * Date: 3/15/2017
 * Time: 9:24 AM
 */

class Grades extends Admin_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('grades_m');
    }
    public function index()
    {
        $this->data['grades'] = $this->grades_m->get();
        $this->data['title'] = $this->lang->line('menu_session');
        $this->data['subview']='grades/index';
        $this->run();

    }
    public function edit($id=null){
        //check a grades new one
        if($id){
            $this->data['grades'] = $this->grades_m->get($id);
            count($this->data['grades'])|| $this->data['errors']='grades could not fine';
        }
        else{
            $this->data['grades'] = $this->grades_m->get_new();
        }
        // Set up the for for input data
        $rules = $this->grades_m->rules;
        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == TRUE) {
            $pastval = $_POST;

            if(isset( $pastval['submit'])){unset( $pastval['submit']);}
//            if(isset( $pastval->submit)){unset( $pastval->submit);}

            $field = field_post2($pastval);
            $data = $this->grades_m->array_from_post($field);
//            echo dump($data);
//                exit();
            $this->grades_m->save($data, $id);
            redirect('grades');
        }

        $this->data['subview']='grades/edit';
        $this->run();
    }
    public function deactive($id = null) {
        //get all data from order
        $grades = $_REQUEST['id'];
        foreach($grades as $item){
            $item;
            $this->grades_m->deactive($item);
        }
    }
}