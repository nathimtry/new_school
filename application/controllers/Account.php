<?php
/**
 * 
 */
class Account extends Admin_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('ledger_m');
        $this->load->helper('form');
        $this->load->model('vouchers_m');
        $this->load->model('voucher_detail_m');
        $this->load->model('chart_of_account_m');
	}
	public function index(){
		//fetch all ledger from db

        $this->data['title'] = 'Leadger';
        $this->data['asset'] = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>3));
        $this->data['liability'] = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>4));
        $this->data['income'] = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>2));
        $this->data['equity'] = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>5));
//        echo dump($this->db->last_query());
//        exit();
        $this->data['expense'] = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>1));
        /*$this->data['asset'] = $this->sum_list($this->asset());
		$this->data['liability'] = $this->sum_list($this->liability());
		$this->data['income'] = $this->sum_list($this->income());
		$this->data['equity'] = $this->sum_list($this->equity());
		$this->data['expense'] = $this->sum_list($this->expense());*/
		// load subview
		$this->data['subview'] = 'account/index';
		$this->run();
	}
	public function profit(){
		//fetch all ledger from db

        $this->data['title'] = 'Profit and Lost';
        $this->data['income'] = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>2));
        $this->data['expense'] = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>1));
        // load subview
        $this->data['subview'] = 'account/profit';
		$this->run();
	}

	public function expense(){
	    $liability = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>1));
        return($liability);

    }
	public function income(){
	    $liability = $this->voucher_detail_m->get_voucher_type(array('chart_of_account.catid'=>2));
        return($liability);

    }
	public function liability(){
	    $liability = $this->voucher_detail_m->get_voucher_type(array('type'=>4));
        return($liability);

    }
    public function asset(){
        $asset = $this->voucher_detail_m->get_voucher_type(array('type'=>3));
        return($asset);
    }
    public function equity(){
        $equity = $this->voucher_detail_m->get_voucher_type(array('type'=>5));
        return($equity);
    }
    public function balance(){
        
    }
	public function deactive($id) {
		$this->ledger_m->deactive($id);
		redirect('ledger');
	}
	public function sum_list($list){
        $balance = 0;
        $debit=0;
        $credit=0;
        foreach($list as $item){
            $debit += $item->debit;
            $credit += $item->credit;
        }
        $balance = $credit-$debit;
        return $balance;
    }
	public function edit($id = Null){
		//check a ledger new one
		if($id){
			$this->data['ledger'] = $this->ledger_m->get($id);
			count($this->data['ledger'])|| $this->data['errors']='ledger could not fine';
		}
		else{
			$this->data['ledger'] = $this->ledger_m->get_new();
		}
		
		//ledgers for dropdown
		$this->data['ledgers'] = $this->ledger_m->get();
		// Set up the for for input data
		$rules = $this->ledger_m->rules;
		$this->form_validation->set_rules($rules);
		
		// Process the form
		if($this->form_validation->run() == TRUE){
		    $field = field_post($_POST);
			$data = $this->ledger_m->array_from_post($field);
			$this->ledger_m->save($data, $id);
			redirect('ledger');
		}
		//load subview
		$this->data['subview'] = 'ledger/edit';
		$this->run();
	}
	public function delete($id) {
		$this->ledger_m->delete($id);
		redirect('ledger');
	}
	
}
