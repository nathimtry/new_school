<?php
function convertText($text){
    if($text<0){
        echo '('.(0-$text).')'; //display minus text as -100 => (100)
    }else{
        echo $text;
    }
}
function to_sqldate($date){
	$todate = date('Y-m-d', strtotime(str_replace('-', '/', $date)));
	return $todate;
}

function show_sqldate($date){
    $todate = date('d-m-Y', strtotime($date));
    return $todate;
}
function print_sqldate($date){
    if($date!='0000-00-00'){
        $todate = date('d-M-Y', strtotime($date));
        return $todate;
    }else{
        return '';
    }
}
function pre_date(){
	return date('Y-m-d', strtotime(' -3 day'));
}
function format_id($id){
    return sprintf('%05d',$id);
}

function loan_status($duration,$remain, $release){
//function loan_status(){
    $today = date('F, Y');
    $date = DateTime::createFromFormat('Y-m-d', $release);
    $status = '';
    if($duration == $remain){
        $status = 'release';
    }elseif($remain == 0){
        $status = 'complete';
    }else{
        $status = 'progress';
    }
    if($today == $date->format('F, Y')){
        $status = 'release';
    }
    return $status;
}
function credit($late_date){
    $credit = array('S','A','B','C','D');
    $x = 0;
    if($late_date>60){
        $x=4;
    }elseif($late_date>30){
        $x=3;
    }elseif($late_date>10){
        $x=2;
    }elseif($late_date>0){
        $x=1;
    }else{
        $x=0;
    }

    return $credit[$x];
//    return $x;
}
function makeoption ($data){
    $x=$data;
    $result =  array();
    foreach ($x as $k =>$item){
        $result[$item->id] = $item->name;
    }
    return $result;
}
function field_post($post_data){
    array_pop($post_data);
    $field = array();
    foreach($post_data as $k => $v){
        array_push($field,$k);
    }
    return $field;
}

function field_post2($post_data){
//    array_pop($post_data);
    $field = array();
    foreach($post_data as $k => $v){
        array_push($field,$k);
    }
    return $field;
}

function khmonth($id){
    $month = array('មករា','កុម្ភៈ','មិនា','មេសា','ឧសភា','មិថុនា',
        'សីហា','កក្កដា','កញ្ញា','តុលា','វិច្ឋិកា','ធ្នូ');
    return $month[$id-1];
}
function nex_date(){
	return date('Y-m-d', strtotime(' +3 day'));
}
function next_payment($start=null, $xdate){
	if ($start == null) {
		$time = time();
	}else{
		$time = strtotime($start);
	}
	//$date = array('d'=>'7', 'm'=>1);
	$nextpay  = date("m/d/Y", mktime(0,0,0,date("n", $time) + $xdate['m'],date("j",$time) + $xdate['d'] ,date("Y", $time)));

	return $nextpay;
}

function get_status($id){
	if($id == null){
		return 0;
	}
	$status = array('ទំនេរ' , 'ទ្រនាប់ដៃ','បង់រំលោះ','លក់ដាច់');
	return $status[$id];
}
function get_block($blocks, $select){
	$str = '';
	foreach($blocks as $block){
		if($block->id == $select){
			$str.= $block->name;
		}
	}
	return $str;
}
function get_select_block($blocks, $select = null){
	$str = '';
	$str .= '<select name="block_id" class="form-control" id="block_id">';
	$str .= '<option value="">Select block</option>';
	//$product = $this->product_m->get();
	//dump($products);
	foreach($blocks as $block){
		if($block->id == $select){
			$str.= '<option value="'.$block->id.'" selected>'.$block->name.'</option>';
		}else{
			$str.= '<option value="'.$block->id.'">'.$block->name.'</option>';
		}

	}
	$str .='</select>';
	return $str;
}


function get_select_product($products,$name, $select = null){
	$str = '';
	$str .= '<select name="'.$name.'" class="form-control" required>';
	$str .= '<option value="">Select</option>';
//	$product = $this->product_m->get();
//	dump($select);
//	exit();
	foreach($products as $product){
		if($select == $product->id){
			$str.= '<option value="'.$product->id.'" selected>'.$product->name.'</option>';
		}else{
			$str.= '<option value="'.$product->id.'">'.$product->name.'</option>';
		}
	}
	 $str .='</select>';
	 return $str;
}
function get_select_category($products, $select = null){
	$str = '';
	$str .= '<select name="category_id" class="form-control">';
	$str .= '<option value="">Select Category</option>';
	//$product = $this->product_m->get();
	//dump($products);
	foreach($products as $product){
		if($select == $product->id){
			$str.= '<option value="'.$product->id.'" selected>'.$product->name.'</option>';
		}else{
			$str.= '<option value="'.$product->id.'">'.$product->name.'</option>';
		}
	}
	$str .='</select>';
	return $str;
}
function get_select_group($array_val,$name, $select = null, $id=null){

	$str = '';

	$str .= '<div class="form-group"><div class="col-md-9 col-sm-9 col-xs-12"><select id="'.$id.'" class="select2_group form-control" name="'.$name.'" onchange="land_ajax(this.value, this)">';
	$str .= '<option></option>';

	//$product = $this->product_m->get();
	//dump($products);
	$temp_block_id = '';
	foreach($array_val as $value){
		if ($value->block_id > $temp_block_id) {
			$str.= '<optgroup label="'.$value->block_name.'">';
			if($select == $value->id){
					$str.= '<option value="'.$value->id.'" selected>'. $value->name.'</option>';
				}else{
					$str.= '<option value="'.$value->id.'">'.$value->name.'</option>';
				}
		}
		else {
			if($select == $value->id){
				$str.= '<option value="'.$value->id.'" selected>'.$value->name.'</option>';
			}else{
				$str.= '<option value="'.$value->id.'">'.$value->name.'</option>';
			}
		}
		$temp_block_id = $value->block_id;

	}
	$str .='</select></div></div>';
	return $str;
}
function get_select_customer($customers,$name, $select = null){
	$str = '';

	$str .= '<div class="form-group customer">
	<div class="col-md-9 col-sm-9 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" name="'.$name.'" id="'.$name.'">';
	$str .= '<option></option>';

	$str .='
	</select>
			
	</div>
	</div>';
	return $str;
}
function get_transaction($transactions,$name, $select = null){
	$str = '';

	$str .= '<div class="form-group">
	<div class="col-md-9 col-sm-9 col-xs-12">
			<select class="select2_single form-control" id="sell_id" tabindex="-1" name="'.$name.'">';
	$str .= '<option></option>';

	//$product = $this->product_m->get();
	//dump($products);
	foreach($transactions as $transaction){
		if($select == $transaction->id){
			$str.= '<option value="'.$transaction->id.'" selected> Invoice '.$transaction->id.' - '.$transaction->mobile_phone.' - '.$transaction->kh_name.'</option>';
		}else{
			$str.= '<option value="'.$transaction->id.'"> Invoice Number '.$transaction->id.' - '.$transaction->mobile_phone.' - '.$transaction->kh_name.'</option>';
		}
	}
	$str .='
	</select>
		
	</div>
	</div>';
	return $str;
}

function notify($title,$message){
    echo " <!-- PNotify -->
 <script>
     $(function(){
         var notice = new PNotify({
             title: '".$title."',
             text: '".$message."',
             buttons: {
                 closer: true,
                 sticker: false
             },
             type: 'success',
             styling: 'bootstrap3'
         });
         notice.get().click(function() {
             notice.remove();
         });
     });
 </script>
 <!-- /PNotify -->";
}

function notify_errors($title,$message){
    echo " <!-- PNotify -->
 <script>
     $(function(){
         var notice = new PNotify({
             title: '".$title."',
             text: '".$message."',
             buttons: {
                 closer: true,
                 sticker: false
             },
             type: 'error',
             styling: 'bootstrap3'
         });
         notice.get().click(function() {
             notice.remove();
         });
     });
 </script>
 <!-- /PNotify -->";
}
function notify_error($message){
    echo " <!-- PNotify -->
 <script>
     $(function(){
         var notice = new PNotify({
             title: 'បញ្ហា',
             text: '".$message."',
             buttons: {
                 closer: false,
                 sticker: false
             },
             type: 'error',
             styling: 'bootstrap3'
         });
         notice.get().click(function() {
             notice.remove();
         });
     });
 </script>
 <!-- /PNotify -->";
}
function get_select_province($province,$form_name,$label, $select = null){
    $str = '';
    $str .= ' <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
		<select name="'.$form_name.'" id="'.$form_name.'" class="select2_single form-control" tabindex="-1" required>';
    $str .= '<option value="">'.$label.'</option>';
    //$product = $this->product_m->get();
    //dump($products);
    foreach($province as $block){
        if($block->id == $select){
            $str.= '<option value="'.$block->id.'" selected>'.$block->name.'</option>';
        }else{
            $str.= '<option value="'.$block->id.'">'.$block->name.'</option>';
        }

    }
    $str .='</select></div></div>';
    return $str;
}
function get_select_destrict($province,$form_name,$label, $select = null){
    $str = '';
    $str .= ' <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
		<select name="'.$form_name.'" id="'.$form_name.'" class="select2_single form-control" tabindex="-1" required>';
    $str .= '<option value="">'.$label.'</option>';
    //$product = $this->product_m->get();
    //dump($products);
    foreach($province as $block){
        if($block->id == $select){
            $str.= '<option value="'.$block->id.'" selected>'.$block->name.'</option>';
        }else{
            $str.= '<option value="'.$block->id.'">'.$block->name.'</option>';
        }

    }
    $str .='</select></div></div>';
    return $str;
}
function get_select_commun($province,$form_name,$label, $select = null){
    $str = '';
    $str .= ' <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
		<select name="'.$form_name.'" id="'.$form_name.'" class="select2_single form-control" tabindex="-1" required>';
    $str .= '<option value="">'.$label.'</option>';
    //$product = $this->product_m->get();
    //dump($products);
    foreach($province as $block){
        if($block->id == $select){
            $str.= '<option value="'.$block->id.'" selected>'.$block->name.'</option>';
        }else{
            $str.= '<option value="'.$block->id.'">'.$block->name.'</option>';
        }

    }
    $str .='</select></div></div>';
    return $str;
}
function sum_list($list){
    $balance = 0;
    $debit=0;
    $credit=0;
    foreach($list as $item){
        $debit += $item->debit;
        $credit += $item->credit;
    }
    $balance = $credit-$debit;
    return $balance;
}
function btn_cancecl($uri){
    $CI =& get_instance();
	$str = '<button onClick="window.location.href = \' '.site_url($uri).'\';return false;" class="btn btn-primary">';
     $str .=    $CI->lang->line('btn_cancel');
     $str .=   '   </button>';
	//$str = '<button onClick="window.location.href = \''.. '\'" class="btn btn-primary">Cancel</button>';
	return $str;
}

function add_meta_title($stirng){
	$CI =& get_instance();
	$CI->data['meta_title'] = e($stirng).' - '. $CI->data['meta_title'];
}
function btn_edit($uri){
	return anchor($uri, '<i class="icon-edit fa fa-edit"></i>');
}
function btn_delete($uri){
	return anchor($uri, '<i class="icon-remove fa fa-close"></i>', array('onclick' => "return confirm('Are you sure to delete this record?')"));
}
function btn_print($uri){
    return anchor($uri, '<i class="icon-print fa fa-print"></i>');
}

function e($string){
	return htmlentities($string);
}
function get_admin_menu($array){
	$CI =& get_instance();
	$str = '';
	$str .= '<ul class="nav">'. PHP_EOL;
	if (count($array)) {
		foreach ($array as $key => $value){
			if(in_array('index',$value)){
				$str .=  '<li><a href='.site_url('admin/'.$key).'>'.$key.'</a></li>';
			}
			//$str .=  '<li><a href='.site_url('admin/'.$key).'>'.$key.'</a></li>';
		}
	}

	$str .= '</ul>' . PHP_EOL;

	return $str;
}
function get_menu ($array, $child = FALSE)
{
	$CI =& get_instance();
	$str = '';

	if (count($array)) {
		$str .= $child == FALSE ? '<ul class="nav">' : '<ul class="dropdown-menu">'. PHP_EOL;

		foreach ($array as $item) {
			$active = FALSE;
			if ($CI->uri->segment(1)== $item['slug']) {
				$active = TRUE;
			}
			elseif($CI->uri->segment(1)=="" && $item['slug']=="/"){
				$active = TRUE;
			}
			else{
				$active = FALSE;
			}
			if (isset($item['children']) && count($item['children'])) {
				$str .= $active ? '<li class="dropdown active">': '<li class="dropdown">';
				$str .= '<a class="dropdown-toggle" data-toggle="dropdown" href="'.site_url(e($item['slug'])).'">'.e($item['title']);
				$str .= '<b class="caret"></b></a>'. PHP_EOL;
				$str .= get_menu($item['children'], TRUE);
			}
			else{
				$str .= $active ? '<li class="active">': '<li>';
				$str .= '<a href="'.site_url($item['slug']).'">'. e($item['title']);
				$str .= '</a>'. PHP_EOL;
			}

			$str .= '</li>' . PHP_EOL;
		}

		$str .= '</ul>' . PHP_EOL;
	}

	return $str;
}

function get_excerpt($article, $numwords = 50){
	$string = '';
	$url = 'article/'. intval($article->id) . '/'. e($article->slug);
	$string .= '<h2>'. anchor($url, e($article->title)). '</h2>';
	$string .= '<p class="pubdate">'. e($article->pubdate).'</p>';
	$string .= '<p>'. e(limit_to_numwords(strip_tags($article->body), $numwords)).'</p>';
	$string .= '<p>'. anchor($url, 'Read more>', array('title' => e($article->title))). '</p>';

	return $string;
}
function article_link($article){
	$url = 'article/'. intval($article->id) . '/'. e($article->slug);
	return $url;
}
function article_links($articles){
	$string = '<ul>';
	foreach ($articles as $article){
		$string .= '<li>'.anchor(article_link($article),e($article->title)).'</li>';
	}
	$string .= '</ul>';
	return $string;
}

function limit_to_numwords($string, $numwords){
	$excerpt = explode(' ', $string, $numwords +1);
	if(count($excerpt) >= $numwords){
		array_pop($excerpt);
	}
	$excerpt = implode(' ', $excerpt);
	return $excerpt;
}


/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {
	function dump ($var, $label = 'Dump', $echo = TRUE)
	{
		// Store dump in variable
		ob_start();
		var_dump($var);
		$output = ob_get_clean();

		// Add formatting
		$output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
		$output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

		// Output
		if ($echo == TRUE) {
			echo $output;
		}
		else {
			return $output;
		}
	}
}
if (!function_exists('dump_exit')) {
	function dump_exit($var, $label = 'Dump', $echo = TRUE) {
		dump ($var, $label, $echo);
		exit;
	}
}
function numberTowords($num)
{
    $ones = array(
        0 => "",
        1 => "One",
        2 => "Two",
        3 => "Three",
        4 => "Four",
        5 => "Five",
        6 => "Six",
        7 => "Seven",
        8 => "Eight",
        9 => "Nine",
        10 => "Ten",
        11 => "Eleven",
        12 => "Twelve",
        13 => "Thirteen",
        14 => "Fourteen",
        15 => "Fifteen",
        16 => "Sixteen",
        17 => "Seventeen",
        18 => "Eighteen",
        19 => "Nineteen"
    );
    $tens = array(
        0 => "",
        1 => "Ten",
        2 => "Twenty",
        3 => "Thirty",
        4 => "Forty",
        5 => "Fifty",
        6 => "Sixty",
        7 => "Seventy",
        8 => "Eighty",
        9 => "Ninety"
    );
    $hundreds = array(
        "Hundred",
        "Thousand",
        "Million",
        "Billion",
        "Trillion",
        "Quadrillion"
    ); //limit t quadrillion
    $num = number_format($num,2,".",",");
    $num_arr = explode(".",$num);
    $wholenum = $num_arr[0];
    $decnum = $num_arr[1];
    $whole_arr = array_reverse(explode(",",$wholenum));
    krsort($whole_arr);
    $rettxt = "";
    foreach($whole_arr as $key => $i){
        if($i < 20){
            $rettxt .= $ones[$i];
        }elseif($i < 100){
            $rettxt .= $tens[substr($i,0,1)];
            $rettxt .= " ".$ones[substr($i,1,1)];
        }else{
            $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
            $rettxt .= " ".$tens[substr($i,1,1)];
            $rettxt .= " ".$ones[substr($i,2,1)];
        }
        if($key > 0){
            $rettxt .= " ".$hundreds[$key]." ";
        }
    }
    if($decnum > 0){
        $rettxt .= " and ";
        if($decnum < 20){
            $rettxt .= $ones[$decnum];
        }elseif($decnum < 100){
            $rettxt .= $tens[substr($decnum,0,1)];
            $rettxt .= " ".$ones[substr($decnum,1,1)];
        }
    }
    return $rettxt;
}
function member_modal(){
    ?>
    <!-- Large modal -->

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">បន្ថែមសមាជិក</h4>
                </div>
                <div class="modal-body">
                    <form role="form" id="member">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-4">
                                <label for="id">អត្តលេខ</label>
                                <input type="text"​ value="" id="alias_id" name="alias_id" placeholder="អត្តលេខ" class="form-control" required>
                            </div>

                            <div class="col-sm-4">
                                <label for="mobile_phone">ទូរស័ព្ទដៃលេខ (*)</label>
                                <input type="text" id="mobile_phone" name="mobile_phone" value="" placeholder="ទូរស័ព្ទដៃលេខ" class="form-control" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="kh_name">ឈ្មោះជាខ្មែរ</label>
                                <input type="text" id="kh_name" name="kh_name" value="" placeholder="ឈ្មោះជាខ្មែរ" class="form-control" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="en_name">ឈ្មោះជាឡាតាំង</label>
                                <input type="text" id="en_name" name="en_name" value="" placeholder="ឈ្មោះជាឡាតាំង" class="form-control">
                            </div>

                            <div class="col-sm-4">
                                <label for="citizen">សញ្ជាតិ</label>
                                <input type="text" id="citizen" name="citizen" value=" " placeholder="សញ្ជាតិ" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                <label for="occupation">មុខរបរ</label>
                                <input type="text" id="occupation" name="occupation" value=" " placeholder="មុខរបរ" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                <label for="dob">ថ្ងៃខែឆ្នាំកំណើត</label>
                                <input type="date" id="dob" name="dob" value=" " placeholder="ថ្ងៃកំណើត" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                <label for="cid">អត្តសញ្ញាណប័ណ្ណលេខ</label>
                                <input type="text" id="cid" name="cid" value=" " placeholder="អត្តសញ្ញាណប័ណ្ណលេខ" class="form-control">
                            </div>

                        </div>


                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="pob">ទីន្លែងកំណើត</label>
                            <input type="text" id="pob" name="pob" value="" placeholder="ទីន្លែងកំណើត" class="form-control">
                        </div>
                        <div class="col-sm-9">
                            <label for="address">អាស័យដ្ឋាន</label>
                            <input type="text" id="address" name="address" value="" placeholder="អាស័យដ្ឋាន" class="form-control">
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-sm-3">
                            <label for="home_phone">ទូរស័ព្ទផ្ទះលេខ</label>
                            <input type="text" id="home_phone" name="home_phone" value="" placeholder="ទូរស័ព្ទផ្ទះលេខ" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="mobile_phone">Facebook</label>
                            <input type="text" id="facebook" name="facebook" value="" class="form-control">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="<?php echo site_url('')?>member/edit" class="btn btn-primary">Full Edit</a>
                    <button type="submit" class="btn btn-primary" id="modal_save">Save changes</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <?php
}
// from number to word library
