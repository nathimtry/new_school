<?php
/**
 * This controller is using for controll all use in admin area
 */
class Admin_Controller extends My_Controller {
	protected $allClass;
	protected $calledMethod;
	protected $isAuthException;
	protected $auth;
	protected $menu = array();
	protected $function;
	public function __construct() {
		parent::__construct();
		$this -> data['meta_title'] = 'Khmer CMS';
		$this -> load -> helper('form');
		$this -> load -> library('session');
		$this -> load -> library('form_validation');
		$this -> load -> model('user_m');
		$this -> load -> model('users_groups_m');
		$this -> load -> model('group_m');
		//$this -> load -> library('router');
        $this->load->helper('km');
		$this->load->model('action_log_m');
		$exception_uris = array('user/login', 'user/logout');
		
		
		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this -> user_m -> loggedin() == FALSE) {
				redirect('user/login');
			}
			else{
			    //check user role


				$this->session->userdata['role'] = $this->getRole($this->session->userdata['id'])->name;
				$feature = $this->user_m->get_query($this->session->userdata['id']);
//				var_dump($this->db->last_query());
//				var_dump($feature);
//                exit();
				$auth = array(
						'dashboard' => array('index'),
						'user' => array('login', 'logout', ),
						'action_log' => array( 'index', 'edit', 'delete'),
						'payment' => array( 'index'),
						'contract' => array( 'index'),
						'product' => array( 'index', 'edit', 'delete'),
						'detail_tran' => array( 'index', 'edit', 'delete'),
						'laundry_income' => array( 'index', 'edit', 'delete'),
						'page' => array( 'index', 'edit', 'delete'),
						'sell' => array( 'index', 'edit', 'delete'),
				);
//				echo serialize($auth);

				//check user role
				$this->authExceptions = unserialize($feature[0]->feature) ; //convert auth from serialize to array
//
//
				$this->data['menu']= $this->authExceptions ; //assign menu
//				$this->calledClass = $this->router->fetch_class(); // check the class name
//				$this->calledMethod = $this->router->fetch_method();
//				$this->auth = array_key_exists($this->calledClass,$this->authExceptions) && in_array($this->calledMethod, $this->authExceptions[$this->calledClass]);
//                var_dump($this->data['menu']);
                foreach($this->data['menu'] as $menu => $function){
//                    var_dump($menu);
                    array_push($this->menu, $menu);
//                    var_dump($function);
                }
//                var_dump($this->menu);
//                exit();
				//end check
//				if($this->session->userdata['role'] == 'administrator'){
//					$this->auth =  True;
//				}
//				if($this->auth == false){
//					redirect('user/login');
//				}
				//dump($this->session->userdata);
			}
		
		}
	}
	function getRole($userdata){
		$groups = $this->users_groups_m->get_by(array('user_id' => $userdata));
				foreach ($groups as $group){
					$group;
				}
				$roles = $this->group_m->get( $group->group);
		return $roles;
	}
	function check_permission($function){
        if (in_array($this->uri->segment(2, 0), $function) == FALSE) {
//            var_dump($this->uri->segment(2, 0));
//            echo 'Permission Deny Click to go Back';
            return false;
        }else{
            return true;
        }
    }

}
