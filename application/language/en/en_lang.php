<?php
$lang['menu_dashboard'] = 'Home';
$lang['menu_customer'] = 'Customer';
//admission
$lang['menu_admission'] = 'admission';
$lang['menu_enrollment'] = 'enrollment';
$lang['menu_student'] = 'student';
$lang['menu_receipt'] = 'receipt';
$lang['menu_grade'] = 'grade';
$lang['menu_class'] = 'class';
$lang['menu_session'] = 'session';
$lang['menu_academic_year'] = 'Academic Year';
$lang['menu_van'] = 'Van';

//academic
$lang['menu_academic'] = 'academic';
$lang['menu_certificate'] = 'certificate';
$lang['menu_program'] = 'program';
$lang['menu_subject'] = 'subject';
$lang['menu_exam'] = 'exam';
$lang['menu_student_evaluation'] = 'Evaluation';
$lang['menu_report'] = 'report';

//Services
$lang['menu_services'] = 'services';
$lang['menu_all_service'] = 'Services List';
$lang['menu_paymentoption'] = 'Payment Option';

//stock
$lang['menu_stock'] = 'Stock';
$lang['menu_balance'] = 'Stock Balance';
$lang['menu_item'] = 'Items';
$lang['menu_purchase_invoice'] = 'Purchase Stock In';

//expense
$lang['menu_expense'] = 'Expense';
$lang['menu_expense_list'] = 'List';
$lang['menu_new_expense'] = 'Add New';


//accounting
$lang['menu_accounting'] = 'Accounting';
$lang['menu_chart_of_account'] = 'Chart of Account';
$lang['menu_expense'] = 'Expense';
$lang['menu_ledger'] = 'Ledger';
$lang['menu_balance_sheet'] = 'Balance Sheet';
$lang['menu_profit_lost'] = 'Profit and Lost';



$lang['setting'] = 'Setting';
$lang['user_manage'] = 'User Administration';
$lang['action_log'] = 'Action Log';



$lang['title_add_new'] = 'Add New';
$lang['title_edit'] = 'Edit';


//button
$lang['btn_add'] = 'Add New';
$lang['btn_remove'] = 'Delete';
$lang['btn_save'] = 'Save';
$lang['btn_cancel'] = 'Cancel';
$lang['btn_search'] = 'Search';
$lang['btn_check'] = 'Check';
$lang['btn_print_schedule'] = 'Print Schedule';
$lang['btn_payment'] = 'Payment';
$lang['btn_final_pay'] = 'Final Payment';
$lang['btn_edit'] = 'Edit';
$lang['btn_redo'] = 'Redo';
$lang['btn_void'] = 'Void';
$lang['btn_release'] = 'Release';

$lang['label_start'] ='Start date';
$lang['label_end'] ='End date';
$lang['label_add_new'] ='Add New';


$lang['note_nodata'] ='No Data';
$lang['info_check'] ='Click here to view the payment schedule and monthly payment';

//list table
$lang['ls_no'] = 'No';
$lang['ls_id'] = 'ID';
$lang['ls_name'] = 'Name';
$lang['ls_latin'] = 'Latin Name';
$lang['ls_sex'] = 'Sex';
$lang['ls_male'] = 'Male';
$lang['ls_female'] = 'Female';
$lang['ls_dob'] = 'DOB';
$lang['ls_mobile'] = 'Telephone';
$lang['ls_address'] = 'Address';
$lang['ls_father_name'] = 'father name';
$lang['ls_father_job'] = 'father job';
$lang['ls_father_phone'] = 'father phone';
$lang['ls_mother_name'] = 'mother name';
$lang['ls_mother_job'] = 'mother job';
$lang['ls_mother_phone'] = 'mother phone';
$lang['ls_guardian'] = 'guardian';
$lang['ls_emergency_phone'] = 'emergency phone';
$lang['ls_remark'] = 'remark';
$lang['ls_level'] = 'Level';
$lang['ls_duration'] = 'Duration';

$lang['ls_study_date']='Study date';
$lang['ls_study_time']='Study time';


$lang['ls_photo'] = 'Photo';
$lang['ls_edit'] = 'Edit';
$lang['ls_print'] = 'Print';
$lang['ls_nationality'] = 'Nationality';
$lang['ls_job'] = 'Job';
$lang['ls_identity'] = 'Identity';
$lang['ls_marital'] = 'Marital Status';
$lang['ls_single'] = 'Single';
$lang['ls_married'] = 'Married';
$lang['ls_photo'] = 'Photo';
$lang['ls_pob'] = 'Place of Birth';
$lang['ls_address'] = 'Address';
$lang['ls_home_phone'] = 'Home Phone';
$lang['ls_facebook'] = 'Facebook';

$lang['ls_release'] = 'release';
$lang['ls_seller'] = 'Seller';
$lang['ls_total_int'] = 'Total Int';
$lang['ls_total_print'] = 'Total Print';
$lang['la_amount'] = 'Amount';
$lang['ls_next_int'] = 'Next Int';
$lang['ls_next_print'] = 'Next Print';
$lang['ls_next_pay'] = 'Next Pay';
$lang['ls_term'] = 'Term';
$lang['ls_paid'] = 'Paid';
$lang['ls_outstanding'] = 'Outstanding';
$lang['ls_reamount'] = 'Re-Amount';
$lang['ls_customer'] = 'Customer';
$lang['ls_reference'] = 'Reference';
$lang['ls_product'] = 'Product-No';
$lang['ls_price'] = 'Price';
$lang['ls_fee'] = 'Fee';
$lang['ls_first_pay'] = 'Advance Payment';
$lang['ls_interest_rate'] = 'Interest Rate';
$lang['ls_term'] = 'Term';
$lang['ls_start_payment'] = 'Start Payment';
$lang['ls_print'] = 'Principal';
$lang['ls_interest'] = 'Interest';
$lang['ls_repay'] = 'Repay';
$lang['schedule_title'] = 'Payment Schedule';
$lang['ls_note'] = 'Remark';
$lang['ls_amount'] = 'Amount';
$lang['ls_contract'] = 'CON-Nº';
$lang['ls_due_date'] = 'Due Date';
$lang['ls_day_late'] = 'Date Late';
$lang['ls_loan'] = 'Loan';
$lang['ls_maturity'] = 'Maturity';
$lang['ls_balance_status'] = 'Balance';
$lang['ls_current'] = 'current';
$lang['ls_penalty'] = 'Penalty';
$lang['ls_payment_schedule'] = 'Table Schedule Payment';
$lang['ls_transaction'] = 'Transaction Payment';
$lang['ls_status'] = 'Status';
$lang['ls_payment_date'] = 'Payment Date';
$lang['ls_saving_balance'] = 'Saving Balance';
$lang['ls_account'] = 'Account';
$lang['ls_user'] = 'user';
$lang['ls_reciept'] = 'RC-No';
$lang['ls_ref_num'] = 'RF-No';
$lang['ls_rc_by'] = 'RC-By';
$lang['ls_amount_write_of'] = 'Am Write Off';
$lang['ls_amount_out'] = 'Amount Out';
$lang['ls_print_out'] = 'Prin Out';
$lang['ls_contract_no'] = 'Contract-No';
$lang['ls_officer'] = 'Rap';
$lang['ls_outstadning'] = 'Amount Outstanding';
$lang['ls_printtext'] = 'Print';
$lang['ls_re-peroid'] = 'Re-Peroid';
$lang['ls_penalti'] = 'Penalti';
