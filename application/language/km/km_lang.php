<?php


$lang['menu_dashboard'] = 'ផ្ទាំងគ្រប់គ្រង';
$lang['menu_customer'] = 'អតិថិជន';
//loan
$lang['menu_loan'] = 'កម្ចី';
$lang['menu_all_loan'] = 'បញ្ជីកម្ចី';
$lang['menu_new_loan'] = 'ផ្តល់កម្ចី';
$lang['menu_dueloan'] = 'កម្ចីដល់ពេលបង់';
$lang['menu_all_late'] = 'បញ្ចីកម្ចីយឺត';
$lang['menu_one_month_late'] = 'កម្ចីយឺតមួយខែ';
$lang['menu_two_month_late'] = 'កម្ចីយឺតពីរខែ';
$lang['menu_over_two_late'] = 'កម្ចីយឺតលើសពីពិរខែ';
$lang['menu_paid_loan'] = 'កម្ចីបានបង់';
$lang['menu_penalty'] = 'កម្ចីត្រូវពិន័យ';
$lang['menu_penalty_ready'] = 'កម្ចីបានពិន័យ';
$lang['menu_out_standing'] = 'កម្ចីនៅជំពាក់';


$lang['menu_repayment'] = 'ការបង់ប្រាក់';
//accounting
$lang['menu_accounting'] = 'គណនី';
$lang['menu_chart_of_account'] = 'តារាងគណនី';
$lang['menu_expense'] = 'បញ្ជីចំណាយ';
$lang['menu_ledger'] = 'ទិន្នានុប្បវត្ត';
$lang['menu_balance_sheet'] = 'បញ្ជីតុល្យការណ៍';
$lang['menu_profit_lost'] = 'ចំណេញខាត';


$lang['menu_report'] = 'របាយការណ៍';
$lang['menu_customer_balance'] = 'សមតុល្យកម្ចីអតិថិជន';

$lang['setting'] = 'កំណត់';
$lang['user_manage'] = 'គ្រប់គ្រងអ្នកប្រើប្រាស់';
$lang['action_log'] = 'សកម្មភាពអ្នកប្រើ';

//member view
$lang['member_title'] = 'បញ្ជីឈ្មោះអតិថិជន';
$lang['contract_title'] = 'បញ្ជីកិច្ចសន្យារំលោះ';
$lang['dueloan_title'] = 'បញ្ជីអតិថិជនដល់ពេលត្រូវបង់ប្រាក់';
$lang['late_list_title'] = 'បញ្ចីអតិថិជនយឺត';
$lang['loan_detail_title'] = 'កិច្ចសន្យា-ព័ត៌មាន';
$lang['schedule_pay_title'] = 'បង់ប្រាក់តាមកាលវិភាគ';
$lang['repay_title'] = 'កម្មវិធីបង់ប្រាក់';

$lang['tittle_add_new'] = 'បង្កើតថ្មី';
$lang['title_edit'] = 'កែប្រែ';

//button
$lang['btn_add'] = 'បន្ថែម';
$lang['btn_remove'] = 'លុប';
$lang['btn_save'] = 'រក្សាទុក';
$lang['btn_cancel'] = 'លះបង់';
$lang['btn_search'] = 'ស្វែងរក';
$lang['btn_check'] = 'ត្រួតពិនិត្យ';
$lang['btn_print_schedule'] = 'បោះពុម្ភៈកាលវិភាគ';
$lang['btn_payment'] = 'បង់ប្រាក់';
$lang['btn_final_pay'] = 'បង់ផ្តាច់';
$lang['btn_edit'] = 'កែប្រែ';
$lang['btn_redo'] = 'ធ្វើឡើងវិញ';
$lang['btn_void'] = 'លុប';

$lang['label_start'] ='ថ្ងៃចាប់ផ្តើម';
$lang['label_end'] ='ថ្ងៃចុងគ្រា';
$lang['label_add_new'] ='បង្កើតថ្មី';


$lang['note_nodata'] ='គ្មានទិន្នន័យ!';
$lang['info_check'] ='Click here to view the payment schedule and monthly payment';

//list table
$lang['ls_no'] = 'ល.រ';
$lang['ls_id'] = 'អត្ថលេខ';
$lang['ls_name'] = 'ឈ្មោះ';
$lang['ls_latin'] = 'ឈ្មោះជាអក្សរឡាតាំង';
$lang['ls_sex'] = 'ភេទ';
$lang['ls_male'] = 'ប្រុស';
$lang['ls_female'] = 'ស្រី';
$lang['ls_dob'] = 'ថ្ងៃកំណើត';
$lang['ls_mobile'] = 'ទូរស័ព្ទ';
$lang['ls_address'] = 'អាស័យដ្ឋាន';
$lang['ls_father_name'] = 'ឪពុកឈ្មោះ';
$lang['ls_father_job'] = 'ឪពុកមុខរបរ';
$lang['ls_father_phone'] = 'លេខទំនាក់ទំនងឪពុក';
$lang['ls_father_phone'] = 'father phone';
$lang['ls_mother_name'] = 'ម្តាយឈ្មោះ';
$lang['ls_mother_job'] = 'ម្តាយមុខរបរ';
$lang['ls_mother_phone'] = 'លេខទំនាក់ទំនងម្តាយ';
$lang['ls_guardian'] = 'អាណាព្យាបាល';
$lang['ls_emergency_phone'] = 'លេខទំនាក់ទំនងបន្ទាន់';
$lang['ls_remark'] = 'ការកត់សម្គាល់';
$lang['ls_study_date']='ថ្ងៃសិក្សា';
$lang['ls_study_time']='ម៉ោងសិក្សា';
$lang['ls_level'] = 'កំរិត';
$lang['ls_duration'] = 'រយៈពេល';



$lang['ls_photo'] = 'រូបថត';
$lang['ls_edit'] = 'កែប្រែ';
$lang['ls_print'] = 'បោះពុម្ព';
$lang['ls_nationality'] = 'សញ្ជាតិ';
$lang['ls_job'] = 'មុខរបរ';
$lang['ls_identity'] = 'អត្តសញ្ញាណប័ណ្ណលេខ';
$lang['ls_marital'] = 'ស្ថានភាពគ្រួសារ';
$lang['ls_single'] = 'នៅលីវ';
$lang['ls_married'] = 'រៀបការ';
$lang['ls_photo'] = 'រូបថត';
$lang['ls_pob'] = 'ទីកន្លែងកំណើត';
$lang['ls_address'] = 'អាស័យដ្ឋាន';
$lang['ls_home_phone'] = 'ទូរស័ព្ទផ្ទះ';
$lang['ls_facebook'] = 'Facebook';

$lang['ls_release'] = 'ថ្ងៃបញ្ចេញ';
$lang['ls_seller'] = 'អ្នកលក់';
$lang['ls_total_int'] = 'សរុបការប្រាក់';
$lang['ls_total_print'] = 'សរុបប្រាក់ដើម';
$lang['la_amount'] = 'ទឹកប្រាក់';
$lang['ls_next_int'] = 'ការប្រាក់ត្រូវបង់';
$lang['ls_next_print'] = 'ប្រាក់ដើមត្រូវបង់';
$lang['ls_next_pay'] = 'ប្រាក់ត្រូវបង់';
$lang['ls_term'] = 'កាលវិភាគ';
$lang['ls_paid'] = 'ប្រាក់បានបង់';
$lang['ls_outstanding'] = 'ប្រាក់នៅសល់';
$lang['ls_reamount'] = 'ទឹកប្រាក់បង់ត្រឡប់';
$lang['ls_customer'] = 'អតិថិជន';
$lang['ls_reference'] = 'សាក្សី';

$lang['ls_product'] = 'ផលិតផល';
$lang['ls_price'] = 'តម្លៃ';
$lang['ls_fee'] = 'ឈ្នួល';
$lang['ls_first_pay'] = 'ទឹកប្រាក់បង់ដំបូង';
$lang['ls_interest_rate'] = 'អត្រាការប្រាក់';
$lang['ls_term'] = 'រយៈពេលរំលោះ/ខែ';
$lang['ls_start_payment'] = 'ថ្ងៃបង់ដំបូង';
$lang['ls_print'] = 'ប្រាក់ដើម';
$lang['ls_interest'] = 'ការប្រាក់';
$lang['ls_repay'] = 'ប្រាក់ត្រូវបង់';
$lang['schedule_title'] = 'តារាងបង់ប្រាក់';
$lang['ls_note'] = 'ចំណាំ';
$lang['ls_amount'] = 'ចំនួនទឹកប្រាក់';
$lang['ls_contract'] = 'លេខកិច្ចសន្យា';
$lang['ls_due_date'] = 'ថ្ងៃត្រូវបង់ប្រាក់';
$lang['ls_day_late'] = 'ថ្ងៃយឺត';
$lang['ls_maturity'] = 'បង់រួច';
$lang['ls_balance_status'] = 'សមតុល្យ';
$lang['ls_current'] = 'បច្ចុប្បន្ន';
$lang['ls_penalty'] = 'ប្រាក់ពិន័យ';
$lang['ls_payment_schedule'] = 'កាលវិភាគបង់ប្រាក់';
$lang['ls_transaction'] = 'ប្រតិបត្តិការបង់ប្រាក់';
$lang['ls_status'] = 'ស្ថានភាព';
$lang['ls_payment_date'] = 'ថ្ងៃបង់ប្រាក់';
$lang['ls_saving_balance'] = 'ប្រាក់សន្សំ';