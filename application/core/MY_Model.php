<?php
class MY_Model extends CI_Model
{

    protected $_table_name = '';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = '';
    protected $_myview = '';
    public $rules = array();
    protected $_timestamps = TRUE;
    protected $_log_table = 'action_log';
    public $table = 'users';

    function __construct() {
        parent::__construct();
    }

    public function array_from_post($fields){
        $data = array();
        foreach($fields as $field){
            $data[$field] = $this->input->post($field);
        }
        return $data;
    }
    public function record_count() {
        return $this->db->count_all($this->_table_name);
    }

    public function fetch_myrecord($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->_table_name);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function get_view($id = null, $single = FALSE){
        if($id !=Null){
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);
            $method = 'row';
        }
        elseif($single == TRUE){
            $method = 'row';
        }
        else{
            $method = 'result';
        }

        if (!count($this->db->order_by($this->_order_by))) {
            $this->db->order_by($this->_order_by);
        }
        return $this->db->get($this->_table_name)->$method();
    }
    public function get_byview($where , $signle = False){
        $this->db->where($where);
        return $this->get_view(null, $signle);

    }

    public function get_like($where , $signle = False){
        $this->db->like($where);
        return $this->get(null, $signle);

    }
    public function get($id = null, $single = FALSE){
        if($id !=Null){
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);

            $method = 'row';
        }
        elseif($single == TRUE){
            $method = 'row';
        }
        else{
            $method = 'result';
        }
        $this->db->where(array('active <>' => '0'));
        if (!count($this->db->order_by($this->_order_by))) {
            $this->db->order_by($this->_order_by);
        }
        return $this->db->get($this->_table_name)->$method();
    }

    public function get_by($where , $signle = False){
        $this->db->where($where);
        return $this->get(null, $signle);

    }
    public function get_last_by($where , $signle = False){
        $this->db->where($where);
        $this->db->order_by($this->_order_by,'DESC');
        return $this->get(null, $signle);

    }
    public function save($data, $id = NULL){
        //timestamp
        if($this->_timestamps == TRUE){
            $now = date('Y-m-d h:i:s');
            $id || $data['created'] = $now;
            $data['modified'] = $now;
            $data['create_by'] = $this->session->userdata['id'];

            $data2['created'] = $now;
            $data2['modified'] = $now;
        }

        $data2['create_by'] = $this->session->userdata['id'];
        //insert
        if ($id === NULL) {
            !isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
            $this->db->set($data);
            $this->db->insert($this->_table_name);
            $id = $this->db->insert_id();
            //add log of information
            $data2['table_name'] = $this->_table_name;
            $data2['action'] = 'create';
            $data2['old_data'] = $this->db->insert_id();

            $this->db->set($data2);
            $this->db->insert($this->_log_table);
        }
        //update
        else{
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name);
            //add log of information
            $data2['table_name'] = $this->_table_name;
            $data2['action'] = 'Update';
            $data2['old_data'] = $id;

            $this->db->set($data2);
            $this->db->insert($this->_log_table);
        }
        return $id;
    }
    public function delete($id){
        $filter = $this->_primary_filter;
        $id = $filter($id);
        if (!$id) {
            return FALSE;
        }
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $this->db->delete($this->_table_name);
    }
    public function re_active($id){
        $filter = $this->_primary_filter;
        $id = $filter($id);
        if (!$id) {
            return FALSE;
        }
        $this->db->set(array('active'=>1));
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $this->db->update($this->_table_name);
    }
    public function deactive($id){
        $filter = $this->_primary_filter;
        $id = $filter($id);
        if (!$id) {
            return FALSE;
        }
        $this->db->set(array('active'=>0));
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $this->db->update($this->_table_name);
    }


    public function deletes($array){
        //todo make deactive for multiple record with array condition
        $this->db->set(array('active'=>0));
        $this->db->where($array);
        $this->db->delete($this->_table_name);
    }
    public function deletes_like($array){
        //todo make deactive for multiple record with array condition
        $this->db->set(array('active'=>0));
        $this->db->like($array);
        $this->db->delete($this->_table_name);
    }
    public function delete_rec($array){
        //todo make deactive for multiple record with array condition
        $this->db->set(array('active'=>0));
        $this->db->where($array);
        $this->db->delete($this->_table_name);
    }
    public function generate_id($id = null){
        $id .= date("Y");
        $id .= date("md");
        $id .= sprintf("%'.06d", count($this->get()) +1);
        return $id;
    }
    public function generate_cid($id = null){
        $id .= sprintf("%'.06d", count($this->get()) +1);
        return $id;
    }

    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    /*
     * Count all records
     */
    public function countAll(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData){

        $this->db->from($this->table);

        $i = 0;
        // loop searchable columns
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }

                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
}