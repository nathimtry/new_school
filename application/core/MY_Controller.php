<?php
class MY_Controller extends CI_Controller
{
	
	public $data = array();
	public function __construct(){
		parent::__construct();
		$this->data['error'] = 'site not found';
		$this->data['subview'] = 'welcome';
		$this->data['meta_title'] = config_item('site_name');
		$this->load->model('MY_Model');
        $this->load->library('session');
		$this->data['languages'] =  array('en','km');
        if($this->session->has_userdata('language')){
//            $this->session->set_userdata('language', 'english');
        }else{
            $this->session->set_userdata('language', 'en');
        }
        $this->session->userdata('language');
        $idiom = $this->session->get_userdata('language');
        $this->lang->load($idiom['language'], $idiom['language']);

	}
	function index(){
		$this->run();
	}
	public function run(){
		$this->load->view('_page_layout', $this->data);
	}
	//for page have dialog box pop use with ajax
	public function run_dialog(){
		$this->load->view('_page_dialog_layout', $this->data);
	}
    //for page have button print location of print text
    public function run_print(){
		$this->load->view('_print_layout', $this->data);
	}
    //for page have button search or can put layout search then print layout with their own print area example balance sheet
    public function run_print_search(){
		$this->load->view('_print_search_layout', $this->data);
	}
	public function run_model(){
		$this->load->view('_modal_layout', $this->data);
	}
    function change_language($lang){
        $language = $this->data['languages'];
        if(in_array($lang,$language)){
            $this->session->set_userdata('language',$lang);
            redirect(site_url($this->uri->segment(1)));
        }else{
            $idiom = $this->session->get_userdata('language');
            $this->lang->load($idiom['language'], $idiom['language']);
            $this->data['message']=$this->lang->line('error_message');
            $this->load->view('error',$this->data);
        }
    }
}