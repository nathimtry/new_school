 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
	<h2><?php echo empty($group->id)? 'Add new group' : 'Edit group: '. $group->name; ?></h2>
	
	</div>
	<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form group Information</h2>
	<?php echo $this->session->flashdata('error');?>
<!--	--><?php //echo validation_errors(); ?>
	<?php echo form_open(); ?>
	<?php //dump($roles);?>
	<table class="table">
		<tr>
			<td>
				Name
			</td>
			<td>
                <?php echo validation_errors('name'); ?>
				<?php echo form_input('name', set_value('name', $group->name),'class="form-control" placeholder="Name"'); ?>
			</td>
		</tr>
        <tr>
            <td>Feature List <input type="checkbox" class="function" id="selecctall"/> Toggle All<br/></td>
            <td>
                <table id="function_table">
                    <?php
                    foreach($controllers as $controller => $item){
                        ?>
                        <tr>
                            <td>
                                <?php
                                echo '<label>'.$controller.'</label>';
                                echo '<input type="checkbox" class="function checkrow">';
                                ?>
                            </td>
                            <td>
                                <?php
                                echo '<label>'.$controller.'</label>';
                                echo '<br>';
                                foreach($item as $k){
                                    echo '<label>'.$k.'</label>';
                                    echo '<input type="checkbox" class="function" name="feature['.$controller.'][]" value="'.$k.'">';
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>

            </td>
        </tr>
		<tr>
			<td>
				
			</td>
			<td>
				<?php echo form_submit('submit', 'Save', 'class="btn btn-success"'); ?><?php echo btn_cancecl($this->uri->segment(1));?>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
 <script language="JavaScript">
     $(document).ready(function () {
         $('#selecctall').click(function (event) {
             if (this.checked) {
                 $('.function').each(function () { //loop through each checkbox
                     $(this).prop('checked', true); //check
                 });
             } else {
                 $('.function').each(function () { //loop through each checkbox
                     $(this).prop('checked', false); //uncheck
                 });
             }
         });
         $('.checkrow').click(function (event) {
             var checkedStatus = this.checked;
             $(this).closest('tr').find(':checkbox').each(function() {
                 $(this).prop('checked', checkedStatus);
             });
         });
     });

 </script>