
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>groups </h3>
                  <small><?php echo anchor('group/edit', '<i class="icon-plus"></i>New Group'); ?></small>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                  <?php echo $this->session->flashdata('error');?>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-striped">
		<thead>
			<th>Name</th>
			<th>Edite</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<?php
//            var_dump($groups);
            if(count($groups)): foreach($groups as $group):
			?>

			<tr>
				<td><?php echo anchor('group/edit/' . $group -> id, $group -> name);?></td>
				<td><?php echo btn_edit('group/edit/' . $group -> id);?></td>
				<td><?php if($group->id != $this->session->groupdata['id']){
					echo btn_delete('group/delete/' . $group -> id);
					}
					?></td>
			</tr>
			<?php	endforeach;?>
			<?php else:?>
			<tr>
				<td colspan="3">There is no record found!</td>
			</tr>
			<?php	endif;?>
		</tbody>
	</table>

                  </div>
                </div>
              </div>
           </div>
         </div>
       </div>