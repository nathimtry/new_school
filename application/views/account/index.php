
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Balance Sheet</h3>
              </div>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <h5 style="text-align: center">Balance Sheet</h5>
                      <h6 style="text-align: center">Date</h6>
                      <table class="table table-responsive col-sm-6">
                          <tr>
                              <th>&nbsp;</th>
                              <th>Total</th>
                          </tr>
                          <tr>
                              <th  width="60%">
                                  Asset
                              </th>
                              <td class="align-right">

                              </td>
                          </tr>
                          <?php
                          $sum=0;
                          foreach($asset as $item){
//                              echo dump($item);
                              $sum +=$item->sum_debit-$item->sum_credit;
                              $temsum = $item->sum_debit-$item->sum_credit;
                              if($temsum!=0):
                              ?>
                              <tr>
                                  <td  width="60%">
                                      <?php
                                      echo '&nbsp;&nbsp; - &nbsp;'. $item->name;
                                      ?>
                                  </td>
                                  <td class="align-right">
                                      <?php
                                      echo convertText($item->sum_debit-$item->sum_credit);
                                      ?>
                                  </td>
                              </tr>
                          <?php
                                endif;
                          }
                          ?>
                          <tr>
                              <th>Total Asset</th>
                              <th class="align-right">
                                  <?php
                                  echo convertText($sum);
                                  ?>
                              </th>
                          </tr>

                      </table>
                      <table class="table  col-sm-6">
                          <tr>
                              <th width="60%">
                                  Liability
                              </td>
                              <td class="align-right">
                                  <?php
//                                  echo $liability;
                                  ?>
                              </td>
                          </tr>
                          <?php
                          $sum_liabi=0;
                          foreach($liability as $item){
//                              echo dump($item);
                              $sum_liabi +=$item->sum_credit-$item->sum_debit;
                              ?>
                              <tr>
                                  <td  width="60%">
                                      <?php
                                      echo '&nbsp;&nbsp; - &nbsp;'. $item->name;
                                      ?>
                                  </td>
                                  <td class="align-right">
                                      <?php
                                      echo convertText($item->sum_credit-$item->sum_debit);
                                      ?>
                                  </td>
                              </tr>
                              <?php
                          }
                          ?>
                          <tr>
                              <td width="60%">
                                  Total Liability
                              </td>
                              <td class="align-right">
                                  <?php
                                  echo $sum_liabi;
                                  ?>
                              </td>
                          </tr>
                          <tr>
                              <th>
                                  Equity
                              </th>
                              <td class="align-right">
                                  <?php
//                                  echo $equity;
                                  ?>
                              </td>
                          </tr>
                          <?php
                          
                          $sum_equity=0;
                          foreach($equity as $item){
//                              echo dump($item);
                              $sum_equity +=$item->sum_credit-$item->sum_debit;
                              ?>
                              <tr>
                                  <td  width="60%">
                                      <?php
                                      echo '&nbsp;&nbsp; - &nbsp;'. $item->name;
                                      ?>
                                  </td>
                                  <td class="align-right">
                                      <?php                                      
                                      convertText($item->sum_credit-$item->sum_debit);
                                      ?>
                                  </td>
                              </tr>
                              <?php
                          }
                          ?>
                          <tr>
                              <td width="60%">
                                  Total Equity
                              </td>
                              <td class="align-right">
                                  <?php
                                  echo convertText($sum_equity);
                                  ?>
                              </td>
                          </tr>
                          <tr>
                              <th>
                                  Net Income
                              </th>
                              <td class="align-right">
                                  <?php
                                  $sum_income=0;
                                  foreach($income as $item){
                                  //                              echo dump($item);
                                      $sum_income +=$item->sum_credit-$item->sum_debit;

                                  }
                                  $sum_expense=0;
                                  foreach($expense as $item){
                                      //                              echo dump($item);
                                      $sum_expense +=$item->sum_credit-$item->sum_debit;

                                  }
                                  echo convertText($sum_income+$sum_expense);

                                  ?>
                              </td>
                          </tr>
                          <tr>
                              <th>Total Liability and Equity</th>
                              <th class="align-right">
                                  <?php
                                  echo convertText($sum_liabi+$sum_equity+$sum_income+$sum_expense);
                                  ?>
                              </th>
                          </tr>
                      </table>
                      

                  </div>
                </div>
              </div>
           </div>
         </div>
       </div>