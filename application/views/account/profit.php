
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $title;?></h3>
              </div>

              
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <h5 style="text-align: center">Profit Report</h5>
                      <h6 style="text-align: center">Date</h6>
                      <table class="table table-responsive col-sm-6">
                          <tr>
                              <th>&nbsp;</th>
                              <th>Total</th>
                          </tr>
                          <tr>
                              <td  width="60%">
                                  Income
                              </td>
                              <td class="align-right">

                              </td>
                          </tr>
                          <?php
                          $sum_income=0;
                          $CI =& get_instance();
                          foreach($income as $item){
                              $account = $CI->chart_of_account_m->get($item->acc_code);
//                              echo $account->name .' '. $item->amount;
                              $sum_income +=$item->sum_credit-$item->sum_debit;
                              if($item->amount!=0 || $item->amount!=''){
                                  ?>
                                  <tr>
                                      <td>&nbsp; &nbsp;<?php echo $account->name;?></td>
                                      <td><?php echo convertText($item->sum_credit);?></td>
                                  </tr>
                                  <?php
                              }
//                              echo dump($item->sum_debit);
                          }
                          ?>
                          <tr>
                              <th>Net Incomem</th>
                              <th class="align-right">
                                  <?php
                                  echo convertText($sum_income);
                                  ?>
                              </th>
                          </tr>

                      </table>
                      <table class="table  col-sm-6">
                          <tr>
                              <td width="60%">
                                  Expense
                              </td>
                              <td class="align-right">
                                  <?php
                                  $sum_expense=0;
                                  foreach($expense as $item){
                                      //                              echo dump($item);
                                      $sum_expense +=$item->sum_credit-$item->sum_debit;

                                  }
                                  echo convertText($sum_expense);
                                  ?>
                              </td>
                          </tr>

                          <tr>
                              <th>Total Liability and Equity</th>
                              <th class="align-right">
                                  <?php
                                  echo convertText($sum_income+$sum_expense);
                                  ?>
                              </th>
                          </tr>
                      </table>
                      

                  </div>
                </div>
              </div>
           </div>
         </div>
       </div>