<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h2>
                កំណត់
                </h2>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="container x_panel">
                        <!--<div class="bigwrapper"> -->
                        <div id="wrapper_1">
                            <?php
                            if($this->session->flashdata('error') and validation_errors()){
                                $str =validation_errors();
                                $str = str_replace("\n", '<br>', $str);
                                $str = str_replace("<p>", '', $str);
                                $str = str_replace("</p>", '', $str);
                                notify($this->session->flashdata('error').'<br>'.$str);
//                echo $str;
                            }
                            if($this->session->flashdata('success')){
                                $str =validation_errors();
                                $str = str_replace("\n", '<br>', $str);
                                $str = str_replace("<p>", '', $str);
                                $str = str_replace("</p>", '', $str);
                                notify('Success',$this->session->flashdata('success').'<br>'.$str);
                            }
//                            echo dump($setting);
                            ?>
                            <?php echo form_open_multipart();?>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-12">

                                    </div>
                                    <div class="col-md-12 row_block">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>ជំរើស</th>
                                                <th>កំណត់</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                        <?php
                                        if (isset($item_setting)) {
//                                            echo dump($capital_account);

                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $item_setting[0]->label?>
                                                    <input type="hidden" name="item[]" readonly value="<?php echo $item_setting[0]->item?>" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text" name="value[]" value="<?php echo $setting[0]->value?>" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $item_setting[1]->label?>
                                                    <input type="hidden" name="item[]" readonly value="<?php echo $item_setting[1]->item?>" class="form-control">
                                                </td>
                                                <td>
                                                    <select name="value[]" id="" class="form-control">
                                                        <option value="">select</option>
                                                        <?php
                                                        if (isset($capital_account)) {
                                                            foreach($capital_account as $account){
                                                                if($setting[1]->value==$account->id){
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>" selected><?php echo $account->name?></option>
                                                                    <?php
                                                                }else{
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>"><?php echo $account->name?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $item_setting[2]->label?>
                                                    <input type="hidden" name="item[]" readonly value="<?php echo $item_setting[2]->item?>" class="form-control">
                                                </td>
                                                <td>
                                                    <select name="value[]" id="" class="form-control">
                                                        <option value="">select</option>
                                                        <?php
                                                        if (isset($expense_account)) {
                                                            foreach($expense_account as $account){
                                                                if($setting[2]->value==$account->id){
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>" selected><?php echo $account->name?></option>
                                                                    <?php
                                                                }else{
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>"><?php echo $account->name?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $item_setting[3]->label?>
                                                    <input type="hidden" name="item[]" readonly value="<?php echo $item_setting[3]->item?>" class="form-control">
                                                </td>
                                                <td>
                                                    <select name="value[]" id="" class="form-control">
                                                        <option value="">select</option>
                                                        <?php
                                                        if (isset($income_account)) {
                                                            foreach($income_account as $account){
                                                                if($setting[3]->value==$account->id){
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>" selected><?php echo $account->name?></option>
                                                                    <?php
                                                                }else{
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>"><?php echo $account->name?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $item_setting[4]->label?>
                                                    <input type="hidden" name="item[]" readonly value="<?php echo $item_setting[4]->item?>" class="form-control">
                                                </td>
                                                <td>
                                                    <select name="value[]" id="" class="form-control">
                                                        <option value="">select</option>
                                                        <?php
                                                        if (isset($cash)) {
                                                            foreach($cash as $account){
                                                                if($setting[4]->value==$account->id){
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>" selected><?php echo $account->name?></option>
                                                                    <?php
                                                                }else{
                                                                    ?>
                                                                    <option value="<?php echo $account->id?>"><?php echo $account->name?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>

                                            <?php
                                            /*foreach($item_setting as $item){
//                                                echo dump($item);
                                                */?><!--
                                                <tr>
                                                    <td>
                                                        <?php /*echo $item->label*/?>
                                                        <input type="hidden" name="item[]" readonly value="<?php /*echo $item->item*/?>" class="form-control">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="value[]" value="<?php /*echo $item->default_value*/?>" class="form-control">
                                                    </td>
                                                </tr>


                                                --><?php
/*                                            }*/
                                        }
                                        ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="col-md-3 offset-md-9">
                                    <div class="form-group">
                                        <?php echo form_submit('submit', 'រក្សាទុក', 'class="btn btn-success"'); ?>
                                        <a href="<?php echo site_url('dashboard')?>" class="btn btn-primary">លះបង់</a>
                                    </div>
                                </div>

                            </div>







                        </div>
                        <?php echo form_close(); ?>
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    //image preview after choose
    window.onload = function(){
        if(window.File && window.FileList && window.FileReader) {
            //Check File API support
            $('.file').on("change", function(event) {
                var _=$(this);
                var files = event.target.files; //FileList object
                for(var i = 0; i< files.length; i++)
                {
                    var file = files[i];
                    //Only pics
                    // if(!file.type.match('image'))
                    if(file.type.match('image.*')){
                        if(this.files[0].size < 10240000){ // 1M
                            // continue;
                            var picReader = new FileReader();
                            picReader.addEventListener("load",function(event){
                                var picFile = event.target;
                                $(_).closest('.thumbnail').find('.pImg').attr('src',picFile.result);
                                $('#image').val(picFile.result);
                                $('#loading').hide();
                            });
                            //Read the image
                            picReader.readAsDataURL(file);
                        }else{
                            alert("Image Size is too big.");
                            $('#loading').hide();
                            $(this).val("");
                        }
                    }else{
                        alert("You can only upload image file.");
                        $(this).val("");
                    }
                }

            });
            $('#loading').hide();
        } else {
            $('#loading').hide();
            console.log("Your browser does not support File API");
        }
    };
    var file = document.getElementsByClassName('file');
    file.onclick = function(){
        document.body.onfocus = function () {
            $('#loading').hide();
            document.body.onfocus = null;
        };
    };
    $(function(){

        $('#loading').hide();
        $('.addstudent').addClass('active');
        $(document).on('click','.pImg', function (e) {
            e.preventDefault();
            $('#loading').show();
            $(this).closest('.thumbnail').find('.file').trigger('click');
        });
        /*$(document).on('submit', function () {
         var _img=$('[name="student_image"]').val();
         if(_img==''){
         info('sm','Information','Please, Choose image','Ok', function () {
         $('.pImg').css('border','#f00 solid 2px');
         });
         return false;
         }
         })*/
    });
</script>