<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h2><?php if (isset($contract)) {
                        echo empty($contract->id)? 'បង្កើតកិច្ចសន្យាទិញ' : 'កែប្រែកិច្ចសន្យាទិញលក់: '. $contract->id;
                    } ?></h2>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="container x_panel">
                        <!--<div class="bigwrapper"> -->
                        <div id="wrapper_1">
                            <?php
                            if($this->session->flashdata('error') and validation_errors()){
                                $str =validation_errors();
                                $str = str_replace("\n", '<br>', $str);
                                $str = str_replace("<p>", '', $str);
                                $str = str_replace("</p>", '', $str);
                                notify($this->session->flashdata('error').'<br>'.$str);
//                echo $str;
                            }
                            if($this->session->flashdata('success')){
                                $str =validation_errors();
                                $str = str_replace("\n", '<br>', $str);
                                $str = str_replace("<p>", '', $str);
                                $str = str_replace("</p>", '', $str);
                                notify('Success',$this->session->flashdata('success').'<br>'.$str);
                            }
                            //        echo dump($user);
                            //        exit();
                            $check = false;
                            if(empty($this->data)){
                                $check=false;
                            }else{
                                if (isset($user)) {
                                    if($user->photo==''){
                                        $check=false;
                                    }else{
                                        if(file_exists('uploads/user/'.$user->photo)){
                                            $check=true;
                                        }else{
                                            $check=false;
                                        }
                                    }
                                }

                            }
                            ?>
                            <?php echo form_open_multipart();?>

                            <div class="row">
                                <div class="col-md-3">
                                    <label for="image">រូបថត</label>
                                    <div class="thumbnail">
                                        <img style="cursor: pointer;" class="img-responsive pImg" src="<?php echo !empty($user->photo)?site_url().'uploads/user/'.$user->photo:site_url().'img/man.png'?>" alt="user profile...">
                                        <input type="file" name="image" style="display: none;" class="file" value="<?php echo !empty($user->photo)?site_url().'uploads/'.$user->photo:site_url().'img/man.png'?>">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-12 row_block">
                                        <div class="form-group">
                                            <label for="name">ឈ្មោះ</label>
                                            <?php echo form_input('name', set_value('name', $user->name),'class="form-control" placeholder="username" id="name"' );?>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">ភេទ</label>
                                            <?php
                                            $options = array(
                                                '1'         => 'Male',
                                                '2'           => 'Female',
                                            );
                                            echo form_dropdown('sex', $options, $user->sex, 'class="form-control"');
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">ថ្ងៃខែឆ្នាំកំណើត</label>
                                            <input type="date" class="form-control" value="<?php echo $user->dob?>" name="dob">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">ទីកន្លែងកំណើត</label>
                                            <?php echo form_input('pob', set_value('pob', $user->pob),'class="form-control" placeholder="pob" id="pob"' );?>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone">លេខទូរស័ព្ទ</label>
                                            <?php echo form_input('phone', set_value('phone', $user->phone),'class="form-control" placeholder="phone" id="phone"' );?>

                                        </div>
                                    </div>
                                    <div class="col-md-12 row_block">
                                        <h5>Login Information</h5>

                                        <div class="form-group">
                                            <label for="name">Email</label>
                                            <input type="email" class="form-control" value="<?php echo $user->email?>" name="email" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Password:</label>
                                            <input type="password" class="form-control" name="password" >

                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Confirm Password:</label>
                                            <input type="password" class="form-control" name="confirm_password" >

                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-3 offset-md-9">
                                    <div class="form-group">
                                        <?php echo form_submit('submit', 'រក្សាទុក', 'class="btn btn-success"'); ?>
                                        <a href="<?php echo site_url('dashboard')?>" class="btn btn-primary">លះបង់</a>
                                    </div>
                                </div>

                            </div>







                        </div>
                        <?php echo form_close(); ?>
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    //image preview after choose
    window.onload = function(){
        if(window.File && window.FileList && window.FileReader) {
            //Check File API support
            $('.file').on("change", function(event) {
                var _=$(this);
                var files = event.target.files; //FileList object
                for(var i = 0; i< files.length; i++)
                {
                    var file = files[i];
                    //Only pics
                    // if(!file.type.match('image'))
                    if(file.type.match('image.*')){
                        if(this.files[0].size < 10240000){ // 1M
                            // continue;
                            var picReader = new FileReader();
                            picReader.addEventListener("load",function(event){
                                var picFile = event.target;
                                $(_).closest('.thumbnail').find('.pImg').attr('src',picFile.result);
                                $('#image').val(picFile.result);
                            });
                            //Read the image
                            picReader.readAsDataURL(file);
                        }else{
                            alert("Image Size is too big.");
                            $(this).val("");
                        }
                    }else{
                        alert("You can only upload image file.");
                        $(this).val("");
                    }
                }

            });
        } else {
            console.log("Your browser does not support File API");
        }
    };

    var file = document.getElementsByClassName('file');
    file.onclick = function(){
        document.body.onfocus = function () {
            document.body.onfocus = null;
        };
    };
    $(function(){

        $('#loading').hide();
        $('.addstudent').addClass('active');
        $(document).on('click','.pImg', function (e) {
            e.preventDefault();
            $(this).closest('.thumbnail').find('.file').trigger('click');
        });
    });
</script>