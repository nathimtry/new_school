 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
	<h2><?php echo empty($user->id)? 'Add new user' : 'Edit user: '. $user->name; ?></h2>
	
	</div>
	<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form User Information</h2>
	<?php echo $this->session->flashdata('error');?>
	<?php echo validation_errors(); ?>
	<?php echo form_open(); ?>
	<?php //dump($roles);?>
	<table class="table">
		<tr>
			<td>
				Name
			</td>
			<td>
				<?php echo form_input('name', set_value('name', $user->name),'class="form-control" placeholder="Name"'); ?>
			</td>
		</tr>
		<tr>
			<td>
				Email
			</td>
			<td>
				<?php echo form_input('email', set_value('email', $user->email),'class="form-control" placeholder="Email"'); ?>
			</td>
		</tr>
		<tr>
			<td>
				Role
			</td>
			<td>
				<select name="role" id="role" class="form-control">
				<?php foreach ($roles as $role):?>
					<?php if ($user->role == $role->name) { ?>
					<option value="<?php echo $role->id;?>" selected><?php echo $role->name?></option>
					<?php }else { ?>
					<option value="<?php echo $role->id;?>"><?php echo $role->name?></option>
				<?php 
 					}
				endforeach;?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Password
			</td>
			<td>
				<?php echo form_password('password','','class="form-control" placeholder="Password"'); ?>
			</td>
		</tr>
		<tr>
			<td>
				Confirm Password
			</td>
			<td>
				<?php echo form_password('password_confirm','','class="form-control" placeholder="Confirm Password"'); ?>
			</td>
		</tr>
		<tr>
			<td>
				
			</td>
			<td>
				<?php echo form_submit('submit', 'Save', 'class="btn btn-success"'); ?><?php echo btn_cancecl($this->uri->segment(1));?>
			</td>
		</tr>
	</table>
	<?php echo form_close(); ?>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
