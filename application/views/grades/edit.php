

<div class="right_col" role="main">

    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $this->lang->line('title_add_new');?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    //    echo dump(validation_errors()) ;
    $x = $errors = $this->form_validation->error_array();
    //    echo dump($x);

    if (validation_errors()!=''){
        foreach($x as $item => $value){
            echo notify_errors($item,$value);
        }
    }
    ?>
    <?php echo form_open_multipart(); ?>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xs-12">
            <div class="x_panel">

                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="x_title">
                        <?php echo form_submit('submit', $this->lang->line('btn_save'), 'class="btn btn-success"'); ?>
                        <?php echo btn_cancecl($this->uri->segment(1));?>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <label for="level"><?php echo $this->lang->line('ls_level');?>*</label>
                            <input type="text" id="level" name="level" value="<?php echo $grades->level ?>" placeholder="<?php echo $this->lang->line('ls_level');?>" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="kh_name"><?php echo $this->lang->line('ls_name');?>*</label>
                            <input type="text" id="kh_name" name="kh_name" value="<?php echo $grades->kh_name ?>" placeholder="<?php echo $this->lang->line('ls_name');?>" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="en_name"><?php echo $this->lang->line('ls_latin');?>*</label>
                            <input type="text" id="en_name" name="en_name" value="<?php echo $grades->en_name ?>" placeholder="<?php echo $this->lang->line('ls_latin');?>" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="duration"><?php echo $this->lang->line('ls_duration');?>*</label>
                            <input type="text" id="duration" name="duration" value="<?php echo $grades->duration ?>" placeholder="<?php echo $this->lang->line('ls_duration');?>" class="form-control">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
