<?php
$this->load->view ( 'component/model_head' );
?>
 <body class="login">
 <div class="row">
     <div class="col-md-5 col-centered">
         <div class="login_wrapper">
             <div class="animate form login_form">
                 <?php $this->load->view($subview); //subview will be load inside our controller?>

             </div>

         </div>
     </div>
 </div>
