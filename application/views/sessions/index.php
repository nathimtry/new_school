<div class="right_col" role="main">
    <div class="">
        <div class="page-title">

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo $title;?></h2>
                        <div class="clearfix"></div>
                        <?php echo $this->session->flashdata('error');?>
                        <a href="<?php echo site_url('sessions/edit')?>" class="btn btn-primary"><i class="fa fa-plus"></i>
                            <?php echo $this->lang->line('btn_add');?></a>
                        <a href="" id="remove" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                            <?php echo $this->lang->line('btn_remove');?></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="mydatatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="checkall" class="check"><?php echo $this->lang->line('ls_no');?></th>
                                <th><?php echo $this->lang->line('ls_id');?></th>
                                <th><?php echo $this->lang->line('ls_name');?></th>
                                <th><?php echo $this->lang->line('ls_latin');?></th>
                                <th><?php echo $this->lang->line('ls_study_date')?></th>
                                <th><?php echo $this->lang->line('ls_study_time')?></th>
                                <th><?php echo "Action";?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach($sessions as $sess){
                                ?>
                                <tr>
                                    <td><input type="checkbox" name="id[]" value=<?php echo $sess->id;?>> <?php echo $i?></td>
                                    <td><?php echo $sess->id;?></td>
                                    <td><?php echo $sess->en_name;?></td>
                                    <td><?php echo $sess->kh_name;?></td>
                                    <td><?php echo $sess->study_date;?></td>
                                    <td><?php echo $sess->study_time;?></td>

                                    <td><a href="<?php echo site_url('sessions/edit/'.$sess->id)?>">
                                            <i class="fa fa-edit"></i>
                                        </a><a href="<?php echo site_url('sessions/sessions_report/'.$sess->id)?>">
                                            <i class="fa fa-print"></i>
                                        </a></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#checkall").on("click", function() {
            var all = $(this);
            $('input:checkbox').each(function() {
                $(this).prop("checked", all.prop("checked"));
            });
        });
        $( "#remove" ).click(function(e) {
            //alert( "Handler for .click() called." );
//                 	  console.log($('#cancel_id').val());
            e.s
            var val = [];
            $('input[name="id[]"]:checkbox:checked').each(function(i){
                val[i] = $(this).val();
            });
            var myjson = {'id':val};
            console.log(myjson);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url();?>sessions/deactive/",
                data: {'id':val},
                success:function(data){
                    console.log(data);
//                                  $('#myModal').modal('hide');
                    window.location.reload();
                },
            });
        });

    })
</script>
<script>
    $(document).ready(function() {
        var handleDataTableButtons = function() {
            if ($("#mydatatable-buttons").length) {
                $("#mydatatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        TableManageButtons.init();
    });
</script>