

<div class="right_col" role="main">

    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $this->lang->line('title_add_new');?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    //    echo dump(validation_errors()) ;
    $x = $errors = $this->form_validation->error_array();
    //    echo dump($x);

    if (validation_errors()!=''){
        foreach($x as $item => $value){
            echo notify_errors($item,$value);
        }
    }
    ?>
    <?php echo form_open_multipart(); ?>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xs-12">
            <div class="x_panel">

                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="x_title">
                        <?php echo form_submit('submit', $this->lang->line('btn_save'), 'class="btn btn-success"'); ?>
                        <?php echo btn_cancecl($this->uri->segment(1));?>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <label for="kh_name"><?php echo $this->lang->line('ls_name');?>*</label>
                            <input type="text" id="kh_name" name="kh_name" value="<?php echo $sessions->kh_name ?>" placeholder="<?php echo $this->lang->line('ls_name');?>" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="en_name"><?php echo $this->lang->line('ls_latin');?>*</label>
                            <input type="text" id="en_name" name="en_name" value="<?php echo $sessions->en_name ?>" placeholder="<?php echo $this->lang->line('ls_latin');?>" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="study_date"><?php echo $this->lang->line('ls_study_date');?>*</label>
                            <input type="text" id="study_date" name="study_date" value="<?php echo $sessions->study_date ?>" placeholder="<?php echo $this->lang->line('ls_study_date');?>" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="study_time"><?php echo $this->lang->line('ls_study_time');?>*</label>
                            <input type="text" id="study_time" name="study_time" value="<?php echo $sessions->study_time ?>" placeholder="<?php echo $this->lang->line('ls_study_time');?>" class="form-control">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
