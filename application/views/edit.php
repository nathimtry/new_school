
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h2><?php echo empty($transaction->id)? 'Add new contract' : 'Edit contract: '. $transaction->id; ?></h2>

			</div>
			<div class="row">
				<div class="col-md-9 col-sm-9 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2 style="text-transform: capitalize;">
	<?php echo $this->session->flashdata('error');?>
	<?php echo validation_errors(); ?></h2>
	<?php echo form_open();
	?>
	<!-- show customer information -->
							<table class="table">
							<tr><td colspan=3 align="right"><?php echo form_submit('submit', 'Save Next', 'class="btn btn-success"'); ?><?php echo btn_cancecl($this->uri->segment(1));?></td></tr>
							<tr><td>
							<?php echo form_input('id', set_value('id', $transaction->id),'class="form-control" placeholder="លេខបុង" readonly="true"' ); ?>
                                </td>
                                <td colspan=2><div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">កាលបរិច្ឋេទ<span
                                                    class="required"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="birthday" value="<?php echo date('m/d/Y');?>"
                                                   class="date-picker form-control col-md-7 col-xs-12"
                                                   required="required" type="text" name="start">
                                        </div>
                                    </div></td>
                            </tr>
								<tr>
									<td>អតិថិជន<?php echo get_select_customer($customers, 'customer_id', $transaction->customer_id);?>
			
			</td>
                                    <td>និង <?php echo get_select_customer($customers, 'customer_2', $transaction->customer_2);?>

                                    </td>
									<td>
                                        <label for="refer_name">សាក្សីឈ្មោះ
                                        </label>
                                            <?php echo form_input('refer_name', set_value('refer_name', $transaction->refer_name),'class="form-control" placeholder="សាក្សី"'); ?>

                                    </td>
								</tr>

								<tr>
									<td colspan=3>
										<!-- Sub form of land data -->
										<table id="landTable" class="table">
											<tr class="row_content">
												<th>ដីឡូដ៍</th>
												<th>អាសយដ្ឋាន</th>
												<th><a href="javascript:void(0);" id="addRow">បន្ថែមជួរ</a></th>
											</tr>
											<!-- Check if there is the update -->
						<?php if(empty($detail_trans)):?>
    <tr class="row_data">
												<td>
    
   <?php echo get_select_group($lands, 'land_id[]');?></td>
												<td><input type="text" placeholder="Address"
													name="address[]" class="address form-control"></td>
											</tr>
 
						 <?php
else :
							$i = 1;
							foreach ( $detail_trans as $detail ) :
								
								?>
 <tr class="row_data">
												<td><input id="<?php echo 'detail_id_'.$i;?>" type="hidden"
													placeholder="detail_id" name="detail_id[]"
													value="<?php echo $detail->id;?>" class="form-control">
							<?php echo get_select_group($lands, 'land_id[]', $detail->land_id,"land_id".$i);?></td>
												<td><input type="text" placeholder="Address"
													name="address[]" value="<?php echo $detail->address;?>"
													id='address' class="address form-control"></td>
												
												<td><a href="javascript:void(0);"
													onclick="return confirm('Do you want to delete this?', deleteRowData(this))"><i
														class="icon-remove fa fa-close"></i></a></td>
											</tr>
  <?php
								$i ++;
							endforeach
							;
							?>
  <?php endif;?>
				</table>
									</td>
								</tr>
                                <tr>
                                    <td><label for="total">តម្លៃលក់ </label>
                                        <?php echo form_input('total', set_value('total', $transaction->total),'class="form-control" placeholder="តម្លៃលក់"'); ?></td>
                                    </td>
                                    <td>
                                        <label for="total_title">តម្លៃលក់ជាអក្សរ</label>
                                        <input type="text" value="<?php echo $transaction->total_title?>" name="total_title" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="deposit_1">ប្រាក់កក់លើកទី១</label>
                                        <input type="text" name="deposit_1" class=" form-control" id="deposit_1" value="<?php echo $transaction->deposit_1?>">
                                    </td>
                                    <td><label for="deposit_1_title">ប្រាក់កក់លើកទី១ ជាអក្សរ</label>
                                        <input type="text" name="deposit_1_title" class=" form-control"  id="deposit_1_title" value="<?php echo $transaction->deposit_1_title?>">
                                    </td>
                                    <td><label for="deposit_1_date">ថ្ងៃបង់ប្រាក់កក់លើកទី១</label>
                                        <input type="text" name="deposit_1_date" class=" form-control datePicker"  id="datePicker" value="<?php echo $transaction->deposit_1_date?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="deposit_2">ប្រាក់កក់លើកទី២</label>
                                        <input type="text" name="deposit_2" class=" form-control"  id="deposit_2" value="<?php echo $transaction->deposit_2?>">
                                    </td>
                                    <td><label for="deposit_2_title">ប្រាក់កក់លើកទី២ជាអក្សរ</label>
                                        <input type="text" name="deposit_2_title" class=" form-control" id="deposit_2_title" value="<?php echo $transaction->deposit_2_title?>">
                                    </td>
                                    <td><label for="deposit_2_date">ថ្ងៃបង់ប្រាក់កក់លើកទី២</label>
                                        <input type="text" name="deposit_2_date" class=" form-control datePicker"  id="datePicker" value="<?php echo $transaction->deposit_2_date?>">
                                    </td>
                                </tr>
								<tr>
                                    <td><label for="end">បង់គ្រប់ចំនួននៅថ្ងៃ</label>
                                        <input type="text" name="end" class=" form-control datePicker"  id="datePicker" value="<?php echo $transaction->end?>">
                                    </td>
									<td><label for="other">ផ្សេងៗ<span
											class="required">*</span>
									</label>
			<?php echo form_input('other', set_value('other', $transaction->other),'class="form-control" placeholder="Other"'); ?></td>

									<td><label for="remark">ចំណាំ<span
											class="required">*</span>
									</label>
			<?php echo form_input('remark', set_value('remark', $transaction->remark),'class="form-control" placeholder="Remark"'); ?></td>

								</tr>

							</table>
	<?php echo form_close(); ?>
	</div>

				</div>
			</div>
</div>
					</div>
		</div>
	</div>
</div>
<script type="text/javascript">
//jquery add row to the table of land
$("#addRow").click(function () {
	var body = $("table#landTable");
	var txt = txt + '<tr><td><?php echo get_select_group($lands, "land_id[]", "land_id");?></td>';
	txt = txt + '<td><input type="text" placeholder="Address" name="address[]" class="address form-control"></td>';
	txt = txt + '<td><a href="javascript:void(0);" onclick="deleteRow(this)"><i class="icon-remove fa fa-close"></i></a></td></tr>';
    body.append(txt);
    //$(body).find("input#customFieldName").focus();
});


$(document).ready(function(){
	$('#total').val(sumVal());
});
// end jquery
// other function is on footer
</script>
