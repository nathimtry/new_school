

<div class="right_col" role="main">

    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $this->lang->line('title_add_new');?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    //    echo dump(validation_errors()) ;
    $x = $errors = $this->form_validation->error_array();
    //    echo dump($x);

    if (validation_errors()!=''){
        foreach($x as $item => $value){
            echo notify_errors($item,$value);
        }
    }
    ?>
    <?php echo form_open_multipart(); ?>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <?php echo form_submit('submit', $this->lang->line('btn_save'), 'class="btn btn-success"'); ?>
                    <?php echo btn_cancecl($this->uri->segment(1));?>
                </div>
                <div class="clearfix"></div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="image"><?php echo $this->lang->line('ls_photo');?></label>
                            <div class="thumbnail">
                                <img style="cursor: pointer;" class="img-responsive pImg" src="<?php echo !empty($student->image)?site_url().'uploads/'.$student->image:site_url().'img/man.png'?>" alt="Feature Image...">
                                <input type="file" name="image" style="display: none;" class="file" value="<?php echo !empty($student->image)?site_url().'uploads/'.$student->image:site_url().'img/man.png'?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-sm-4">
                                <label for="mobile_phone"><?php echo $this->lang->line('ls_no');?> *</label>
                                <input type="text" id="id" name="id" value="<?php echo $student->id?>" placeholder="<?php echo $this->lang->line('ls_no');?>" class="form-control" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="kh_name"><?php echo $this->lang->line('ls_name');?></label>
                                <input type="text" id="kh_name" name="kh_name" value="<?php echo $student->kh_name ?>" placeholder="<?php echo $this->lang->line('ls_name');?>" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                <label for="en_name"><?php echo $this->lang->line('ls_latin');?></label>
                                <input type="text" id="en_name" name="en_name" value="<?php echo $student->en_name ?>" placeholder="<?php echo $this->lang->line('ls_latin');?>" class="form-control">
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="sex"><?php echo $this->lang->line('ls_sex');?></label>
                            <p>
                                <?php echo $this->lang->line('ls_male');?>:
                                <input type="radio" class="" name="sex" id="genderM" value="1" <?php echo $student->sex==1?'checked=""':'' ?> required />
                                <?php echo $this->lang->line('ls_female');?>:
                                <input type="radio" class="" name="sex" id="genderF" value="2" <?php echo $student->sex==2?'checked=""':'' ?>/>
                            </p>
                        </div>
                        <div class="col-sm-4">
                            <label for="dob"><?php echo $this->lang->line('ls_dob');?>:</label>
                            <input type="text" id="datepicker"  name="dob" value="<?php echo show_sqldate($student->dob)?>" placeholder="<?php echo $this->lang->line('ls_dob');?>" class="form-control date-picker">
                        </div>
                        <div class="col-sm-4">
                            <label for="pob"><?php echo $this->lang->line('ls_pob');?></label>
                            <input type="text" id="pob" name="pob" value="<?php echo $student->pob ?>" placeholder="<?php echo $this->lang->line('ls_pob');?>" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="address"><?php echo $this->lang->line('ls_address');?>:</label>
                            <input type="text" id="address" name="address" value="<?php echo $student->address ?>" placeholder="<?php echo $this->lang->line('ls_address');?>" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="father_name"><?php echo $this->lang->line('ls_father_name');?></label>
                            <input type="father_name" id="father_name" name="father_name" value="<?php echo $student->father_name ?>" placeholder="<?php echo $this->lang->line('ls_father_phone');?>" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="father_job"><?php echo $this->lang->line('ls_father_job');?></label>
                            <input type="text" id="father_job" name="father_job" value="<?php echo $student->father_job ?>" placeholder="<?php echo $this->lang->line('ls_father_job');?>" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="father_phone"><?php echo $this->lang->line('ls_father_phone');?></label>
                            <input type="number" id="father_phone" name="father_phone" value="<?php echo $student->father_phone ?>" placeholder="<?php echo $this->lang->line('ls_father_phone');?>" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="mother_name"><?php echo $this->lang->line('ls_mother_name');?></label>
                            <input type="text" id="mother_name" name="mother_name" value="<?php echo $student->mother_name ?>" placeholder="<?php echo $this->lang->line('ls_mother_phone');?>" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="mother_job"><?php echo $this->lang->line('ls_mother_job');?></label>
                            <input type="text" id="mother_job" name="mother_job" value="<?php echo $student->mother_job ?>" placeholder="<?php echo $this->lang->line('ls_mother_job');?>" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="mother_phone"><?php echo $this->lang->line('ls_mother_phone');?></label>
                            <input type="number" id="mother_phone" name="mother_phone" value="<?php echo $student->mother_phone ?>" placeholder="<?php echo $this->lang->line('ls_mother_phone');?>" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="guardian"><?php echo $this->lang->line('ls_guardian');?></label>
                            <input type="text" id="guardian" name="guardian" value="<?php echo $student->guardian ?>" placeholder="<?php echo $this->lang->line('ls_guardian');?>" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="emergency_phone"><?php echo $this->lang->line('ls_emergency_phone');?></label>
                            <input type="number" id="emergency_phone" name="emergency_phone" value="<?php echo $student->emergency_phone ?>" placeholder="<?php echo $this->lang->line('ls_emergency_phone');?>" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="remark"><?php echo $this->lang->line('ls_remark');?></label>
                            <input type="text" id="remark" name="remark" value="<?php echo $student->remark ?>" placeholder="<?php echo $this->lang->line('ls_remark');?>" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script>

    //upload image
    //image preview after choose
    window.onload = function(){
        if(window.File && window.FileList && window.FileReader) {
            //Check File API support
            $('.file').on("change", function(event) {
                var _=$(this);
                var files = event.target.files; //FileList object
                for(var i = 0; i< files.length; i++)
                {
                    var file = files[i];
                    //Only pics
                    // if(!file.type.match('image'))
                    if(file.type.match('image.*')){
                        if(this.files[0].size < 10240000){ // 1M
                            // continue;
                            var picReader = new FileReader();
                            picReader.addEventListener("load",function(event){
                                var picFile = event.target;
                                $(_).closest('.thumbnail').find('.pImg').attr('src',picFile.result);
                                $('#image').val(picFile.result);
                                console.log(picFile.result);
                            });
                            //Read the image
                            picReader.readAsDataURL(file);
                        }else{
                            alert("Image Size is too big.");
                            $(this).val("");
                        }
                    }else{
                        alert("You can only upload image file.");
                        $(this).val("");
                    }
                }

            });
        } else {
            console.log("Your browser does not support File API");
        }
    };
    var file = document.getElementsByClassName('file');
    file.onclick = function(){
        document.body.onfocus = function () {
            document.body.onfocus = null;
        };
    };
    $(function(){

        $('.addPatient').addClass('active');
        $(document).on('click','.pImg', function (e) {
            e.preventDefault();
            $(this).closest('.thumbnail').find('.file').trigger('click');
        });
    });
    //end upload
</script>