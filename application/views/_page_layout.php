<?php
$this->load->view ( 'component/head' );
?>


<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php $this->load->view('component/side_menu')?>


        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <?php
                    $this->load->view('component/head_menu');
                    ?>


                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <?php if (isset($subview)) {
            $this->load->view($subview);
        } ?>
        <?php $this->load->view('component/footer'); ?>
       