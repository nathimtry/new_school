<?php
$this->load->view ( 'component/head' );
?>


<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php $this->load->view('component/side_menu')?>


        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<?php echo site_url()?>img/img.jpg" alt=""><?php echo $this->session->userdata['name'];?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">

                                <li><a href="<?php echo site_url('user/profile/'.$this->session->userdata['id'])?>"><i class="fa fa-user pull-right"></i> Profile</a></li>
                                <li><a href="<?php echo site_url('user/setting')?>"><i class="fa fa-cogs pull-right"></i> Setting</a></li>
                                <li><a href="<?php echo site_url('user/logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-sm-12 col-sm-12 col-xs-12">
                        <div class="x_panel page_print">
                            <div class="x_title">
                                <h2><?php echo $title;?></h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <button type="button" class="btn btn-primary print" onclick="printData('print','<?= site_url();?>')">
                                    <i class="fa fa-print"></i> Print</button>
                                <section class="content invoice" id="print">
                                    <!-- title row -->
                                    <?php if (isset($subview)) {
                                        $this->load->view($subview);
                                    } ?>
                                   <!-- this row will not appear when printing -->

                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <?php $this->load->view('component/footer'); ?>
       