<ul class="nav navbar-nav navbar-right">

    <li class="">
        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="<?php echo site_url()?>img/img.jpg" alt=""><?php echo $this->session->userdata['name'];?>
            <span class=" fa fa-angle-down"></span>
        </a>
        <ul class="dropdown-menu dropdown-usermenu pull-right">

            <li><a href="<?php echo site_url('user/profile/'.$this->session->userdata['id'])?>"><i class="fa fa-user pull-right"></i> Profile</a></li>
            <li><a href="<?php echo site_url('user/setting')?>"><i class="fa fa-cogs pull-right"></i> Setting</a></li>
            <li><a href="<?php echo site_url('user/logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
        </ul>
    </li>
    <li role="presentation" class="dropdown">
        <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
            <?php
            if(isset($this->session->userdata['language'])){
                echo 'Languages: '. $this->session->userdata['language'];
            }else{
                echo 'Languages';
            }
            $url = '';
            if(uri_string()!=''){
                $url = uri_string();
            }else{
                $url = 'home';
            }
            ?>

        </a>
        <ul id="menu1" class="dropdown-menu list-unstyled" role="menu">
            <li><a href="<?php echo site_url($url); ?>/change_language/en">English</a></li>
            <li><a href="<?php echo site_url($url); ?>/change_language/km">Khmer</a></li>
        </ul>
    </li>

</ul>