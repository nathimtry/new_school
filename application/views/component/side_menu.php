<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo site_url('dashboard');?>" class="site_title"><span><?php if (isset($meta_title)) {
                          echo $meta_title;
                      } ?></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">

              </div>
              <div class="profile_info">
                <span>សូមស្វាគមន៍</span>
                <h2><?php echo $this->session->userdata['name'];?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3><?php echo $this->session->userdata['role'];?></h3>
                <ul class="nav side-menu">
                    <?php
                    /*
                     * temporary turn off, need to create menu function
                     * todo make mennu function to show the menu

//                    var_dump($menu);
                    foreach($menu as $item => $key){
                        var_dump($key);
                        ?>
                        <li><a href="<?php echo site_url($item);?>"><i class="fa fa-home"></i><?php echo $item;?></a></li>
                    <?php
                    }
                    */
                    ?>

                <li><a href="<?php echo site_url('dashboard');?>"><i class="fa fa-home"></i><?php echo $this->lang->line('menu_dashboard');?></a></li>


                </li>
                <li><a><i class="fa fa-clone"></i><?php echo $this->lang->line('menu_admission');?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('student/edit');?>"><?php echo $this->lang->line('menu_enrollment');?></a></li>
                        <li><a href="<?php echo site_url('student');?>"><?php echo $this->lang->line('menu_student');?></a></li>
                        <li><a href="<?php echo site_url('receipt');?>"><?php echo $this->lang->line('menu_receipt');?></a></li>
                        <li><a href="<?php echo site_url('grades');?>"><?php echo $this->lang->line('menu_grade');?></a></li>
                        <li><a href="<?php echo site_url('class');?>"><?php echo $this->lang->line('menu_class');?></a></li>
                        <li><a href="<?php echo site_url('sessions');?>"><?php echo $this->lang->line('menu_session');?></a></li>
                        <li><a href="<?php echo site_url('academic_year');?>"><?php echo $this->lang->line('menu_academic_year');?></a></li>
                        <li><a href="<?php echo site_url('van');?>"><?php echo $this->lang->line('menu_van');?></a></li>
                    </ul>
                </li>

                <li><a><i class="fa fa-clone"></i><?php echo $this->lang->line('menu_academic');?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('certificate');?>"><?php echo $this->lang->line('menu_certificate');?></a></li>
                        <li><a href="<?php echo site_url('program');?>"><?php echo $this->lang->line('menu_program');?></a></li>
                        <li><a href="<?php echo site_url('subject');?>"><?php echo $this->lang->line('menu_subject');?></a></li>
                        <li><a href="<?php echo site_url('exam');?>"><?php echo $this->lang->line('menu_exam');?></a></li>
                        <li><a href="<?php echo site_url('evaluation');?>"><?php echo $this->lang->line('menu_student_evaluation');?></a></li>
                        <li><a href="<?php echo site_url('score/report');?>"><?php echo $this->lang->line('menu_report');?></a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-clone"></i><?php echo $this->lang->line('menu_services');?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('services');?>"><?php echo $this->lang->line('menu_all_service');?></a></li>
                        <li><a href="<?php echo site_url('paymentoption');?>"><?php echo $this->lang->line('menu_paymentoption');?></a></li>
                    </ul>
                </li>

                <li><a><i class="fa fa-clone"></i><?php echo $this->lang->line('menu_stock');?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('stock/stock_balance');?>"><?php echo $this->lang->line('menu_balance');?></a></li>
                        <li><a href="<?php echo site_url('stock/items');?>"><?php echo $this->lang->line('menu_item');?></a></li>
                        <li><a href="<?php echo site_url('stock/purchase');?>"><?php echo $this->lang->line('menu_purchase_invoice');?></a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-clone"></i><?php echo $this->lang->line('menu_expense');?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('expense');?>"><?php echo $this->lang->line('menu_expense_list');?></a></li>
                        <li><a href="<?php echo site_url('expense/edit');?>"><?php echo $this->lang->line('menu_new_expense');?></a></li>
                    </ul>
                </li>

                <li><a><i class="fa fa-product-hunt"></i><?php echo $this->lang->line('menu_accounting');?></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('chart_of_account');?>"><i class="fa fa-th-list"></i> <?php echo $this->lang->line('menu_chart_of_account');?></a></li>
                        <li><a href="<?php echo site_url('expense');?>"><i class="fa fa-list-ol"></i><?php echo $this->lang->line('menu_expense');?></a></li>
                        <li><a href="<?php echo site_url('income');?>"><i class="fa fa-list-ol"></i>Other Income</a></li>
                        <li><a href="<?php echo site_url('ledger');?>"><i class="fa fa-list-ol"></i><?php echo $this->lang->line('menu_ledger');?></a></li>
                        <li><a href="<?php echo site_url('ledger/transaction');?>"><i class="fa fa-list-ol"></i>Transaction</a></li>
                        <li><a href="<?php echo site_url('ledger/transaction_list');?>"><i class="fa fa-list-ol"></i>Transfer Fun</a></li>
                        <li><a href="<?php echo site_url('payment/report');?>"><i class="fa fa-list-ol"></i>Collection Report</a></li>
                        <li><a href="<?php echo site_url('account');?>"><i class="fa fa-list-ol"></i><?php echo $this->lang->line('menu_balance_sheet');?></a></li>
                        <li><a href="<?php echo site_url('account/profit');?>"><i class="fa fa-list-ol"></i><?php echo $this->lang->line('menu_profit_lost');?></a></li>

                    </ul>
                </li>
                  <li><a><i class="fa fa-clone"></i><?php echo $this->lang->line('setting');?><span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('user');?>"><?php echo $this->lang->line('user_manage');?></a></li>
                      <li><a href="<?php echo site_url('action_log');?>"><?php echo $this->lang->line('action_log');?></a></li>
                    </ul>
                  </li>
                </ul>
              </div>


            </div>
            <!-- /sidebar menu -->

          </div>
        </div>