<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo site_url('img/system.png');?>" type="image/png" sizes="16x16">

    <title><?php echo $meta_title;?></title>

    <!-- Khmer CMS customer CSS -->
    <link href="<?php echo site_url('css/styles.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('css/print.css');?>" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo site_url('asset/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url('asset/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo site_url('asset/build/css/custom.min.css');?>" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo site_url('asset/vendors/select2/dist/css/select2.min.css')?>" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo site_url('asset/vendors/switchery/dist/switchery.min.css')?>" rel="stylesheet">

    <!-- css for data table -->
    <!-- NProgress -->
    <link href="<?php echo site_url('asset/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo site_url('asset/vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo site_url('asset/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('asset/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('asset/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('asset/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('asset/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css');?>" rel="stylesheet">
    <!-- PNotify -->
    <link href="<?php echo site_url('asset/vendors/pnotify/dist/pnotify.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('asset/vendors/pnotify/dist/pnotify.buttons.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('asset/vendors/pnotify/dist/pnotify.nonblock.css');?>" rel="stylesheet">
    <!-- end data table -->
    <!-- jQuery -->
    <script src="<?php echo site_url('js/jquery-3.1.0.min.js')?>"></script>
    <script src="<?php echo site_url('js/moment.js')?>"></script>
    <script src="<?php echo site_url('asset/vendors/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo site_url('asset/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- FastClick -->
    <script src="<?php echo site_url('asset/vendors/fastclick/lib/fastclick.js')?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo site_url('asset/production/js/moment/moment.min.js');?>"></script>
    <script src="<?php echo site_url('asset/production/js/datepicker/daterangepicker.js');?>"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo site_url('asset/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/jquery.hotkeys/jquery.hotkeys.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/google-code-prettify/src/prettify.js');?>"></script>
    <!-- Switchery -->
    <script src="<?php echo site_url('asset/vendors/switchery/dist/switchery.min.js');?>"></script>
    <!-- Select2 -->
    <script src="<?php echo site_url('asset/vendors/select2/dist/js/select2.full.min.js')?>"></script>

    <!-- script support data table -->
    <!-- NProgress -->
    <script src="<?php echo site_url('asset/vendors/nprogress/nprogress.js');?>"></script>
    <!-- iCheck -->
    <script src="<?php echo site_url('asset/vendors/iCheck/icheck.min.js');?>"></script>
    <!-- Datatables -->
    <script src="<?php echo site_url('asset/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-buttons/js/buttons.flash.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/datatables.net-scroller/js/dataTables.scroller.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/jszip/dist/jszip.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/pdfmake/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/pdfmake/build/vfs_fonts.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/pdfmake/build/vfs_fonts.js');?>"></script>
    <!-- end script library -->
    <!-- jquery.inputmask -->
    <script src="<?php echo site_url('asset/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js');?>"></script>

    <!-- PNotify -->
    <script src="<?php echo site_url('asset/vendors/pnotify/dist/pnotify.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/pnotify/dist/pnotify.buttons.js');?>"></script>
    <script src="<?php echo site_url('asset/vendors/pnotify/dist/pnotify.nonblock.js');?>"></script>

    <script src="<?php echo site_url('tinymce/tinymce.min.js');?>"></script>
    <script src="<?php echo site_url('js/function.js');?>"></script>

    <script src="<?php echo site_url('js/jquery.validate.min.js');?>"></script>

</head>