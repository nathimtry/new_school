<!-- footer content -->
<footer>
    <div class="pull-right">
        Copyright 2020- Develop by <a href="http://www.khmercms.com">Khmer CMS</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<div id="loading" style="
    position: absolute;
    top: 50%;
    left: 40%;
"><img width="200px" src="<?php echo site_url('img/InternetSlowdown_Day.gif')?>"></div>
<!-- Custom Theme Scripts -->
<script src="<?php echo site_url('asset/build/js/custom.min.js')?>"></script>


<!-- bootstrap-daterangepicker -->

<!-- Datatables -->
<script>
    $('#loading').hide();
    jQuery.ajaxSetup({
        beforeSend: function() {
            $('#loading').show();
        },
        complete: function(){
            $('#loading').hide();
        },
        success: function() {}
    });
    $(document).ready(function() {
        var handleDataTableButtons = function() {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    lengthMenu: [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                    ],
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[ 1, 'asc' ]],
            'columnDefs': [
                { orderable: false, targets: [0] }
            ]
        });
        $datatable.on('draw.dt', function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();
    });
</script>
<!-- /Datatables -->
<!-- Select2 -->
<script>

    //    function addRow() {
    //        //alert('add row');
    //        var table = document.getElementById("landTable");
    //        var row = table.insertRow(1);
    //        var cell1 = row.insertCell(0);
    //        var cell2 = row.insertCell(1);
    //        var cell3 = row.insertCell(2);
    //        var cell4 = row.insertCell(3);
    //        var text= '<?php //empty($land)? null: get_select_group($lands, "land_id[]");?>//';
    //        cell1.innerHTML = text;
    //        cell2.innerHTML = '<input type="text" placeholder="Address" name="address[]" class="form-control">';
    //        cell3.innerHTML = '<input type="text" placeholder="Price" id="total" name="price[]" class="form-control">';
    //        cell4.innerHTML = '<input type="text" placeholder="total" id="total" name="total[]" class="form-control"><a href="javascript:void(0);" onclick="deleteRow(this)"><i class="icon-remove fa fa-close"></i></a>';
    //    }

    function deleteRow(row) {
        var i = row.parentNode.parentNode.rowIndex;
        document.getElementById("landTable").deleteRow(i);
    }
    //ajax delte data in the form and database then make land status to free.
    function deleteRowData(row){
        var i = row.parentNode.parentNode.rowIndex;
        var id = 'detail_id_'+ i;
        var txt = $('#detail_id_'+ i).val();
        var land =  $('#land_id'+i).val();
        alert(land);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url();?>/transaction/delete_ajax/",
            data: {'detail_id':txt, 'land_id':land},
            success:function(data){
                document.getElementById("landTable").deleteRow(i);

            },
        });// you have missed this bracket
        return false;
    }
    function sumVal(add_val = null){
        var sum = 0;
        //iterate through each td based on class and add the values
        $("#price").each(function() {
            var value = $(this).val();
            // add only if the value is number
            if(!isNaN(value) && value.length != 0) {
                sum += parseFloat(value);
            }
        });
        if(add_val != null){
            sum = sum + parseFloat(add_val);
        }

        return sum;
    }
    //start ajax get land from database to fill in the form
    function land_ajax(row,id){
        $.ajax({
            type: "POST",
            url: "<?php echo site_url();?>/transaction/land_ajax/",
            data: {'land_id':row},
            success:function(data){
                var json = JSON.parse(data);
                //alert(data);
                var price =  $(id).closest('tr').find('input.mprice').val(json['price']);
                var address = $(id).closest('tr').find('input.address').val(json['address']);
                //alert('price: '+price+' address: '+address);
                $('#total').val(sumVal(json['price']));
            },
        });// you have missed this bracket
        return false;
    }
    //end function get land
    $(document).ready(function() {
        $(".select2_single").select2({
            placeholder: "Select",
            allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
            maximumSelectionLength: 4,
            placeholder: "With Max Selection limit 4",
            allowClear: true
        });
    });
</script>
<!-- /Select2 -->
<script>
    $(document).ready(function() {
        $('#birthday, .date-picker').daterangepicker({
            locale: {
                format: 'DD/MMM/YYYY'
            },
            singleDatePicker: true,
            calender_style: "picker_4"
        });
        $('.datePicker').daterangepicker({
            locale: {
                format: 'DD/MMM/YYYY'
            },
            singleDatePicker: true,
            calender_style: "picker_4"

        });
        $('#next_payment').daterangepicker({
            locale: {
                format: 'DD/MMM/YYYY'
            },
            singleDatePicker: true,
            calender_style: "picker_4"
        });
    });
</script>
</body>
</html>