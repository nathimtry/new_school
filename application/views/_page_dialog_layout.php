<?php
$this->load->view ( 'component/head' );
?>


<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php $this->load->view('component/side_menu')?>


        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<?php echo site_url()?>img/img.jpg" alt=""><?php echo $this->session->userdata['name'];?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">

                                <li><a href="<?php echo site_url('user/profile/'.$this->session->userdata['id'])?>"><i class="fa fa-user pull-right"></i> Profile</a></li>
                                <li><a href="<?php echo site_url('user/setting')?>"><i class="fa fa-cogs pull-right"></i> Setting</a></li>
                                <li><a href="<?php echo site_url('user/logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <?php if (isset($subview)) {
            if(is_array($subview)){
                foreach($subview as $item){
                    $this->load->view($item);
                }
            }else{
                $this->load->view($subview);
            }

        } ?>
        <?php $this->load->view('component/footer'); ?>
       