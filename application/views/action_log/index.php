
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
	<h2>Action Log</h2>
	</div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                  <?php echo $this->session->flashdata('error');?>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
	<table class="table table-striped" id="datatable-buttons" >
		<thead>
			<tr>
				<th>ID</th>
				<th>User</th>
				<th>Action</th>
				<th>Feature</th>
				<th>ID</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($action_logs)): foreach($action_logs as $action_log): ?>	
		<tr>
			<td><?php echo anchor('action_log_income/edit/' . $action_log->id, $action_log->id); ?></td>
			<td><?php echo $action_log->create_by; ?></td>
			<td><?php echo $action_log->action; ?></td>
			<td><?php echo $action_log->table_name; ?></td>
			<td><?php echo $action_log->old_data; ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any pages.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
      </div>
                </div>
              </div>
           </div>
         </div>
       </div>