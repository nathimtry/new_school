
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content text-center text-center">

                        <h1 class="error-number">403</h1>
                        <h2>Access denied</h2>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>