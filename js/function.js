/**
 * Created by bunnaro on 11/27/2019.
 */

function printData(id,pathcss) {
    var mywindow = window.open('', 'PRINT', 'width = 850, height = 700');

    var xcss = "";
    var css = "";
    var pcss = "";
    var myStylesLocation = pathcss+"/css/bootstrap.css";
    var StylesLocation = pathcss+"/css/print.css";
    var pStylesLocation = pathcss+"/css/style.css";

    $.ajax({
        url: myStylesLocation,
        type: "POST",
        async: false
    }).done(function(data){
        css += data;
    });
    $.ajax({
        url: StylesLocation,
        type: "POST",
        async: false
    }).done(function(data){
        xcss += data;
    });
    $.ajax({
        url: pStylesLocation,
        type: "POST",
        async: false
    }).done(function(data){
        pcss += data;
    });

    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('<style type="text/css">'+css+' </style>');
    // mywindow.document.write('<style type="text/css">'+pcss+' </style>');
    mywindow.document.write('<style type="text/css">'+xcss+' </style>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById(id).innerHTML);
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();


    return true;
}
